﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.DUNGEON_SETTINGS;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.GAMEPLAY.DUNGEON{
	public class Enemy : MonoBehaviour {

		public Dungeon dungeon;

		public Enemy_settings enemy_settings;

		public Enemy_object enemy_object;

		public Variable_object_position variable_position;

		public Variable_object_rotation variable_rotation;

		public SAKURA.Consts.Gameplay.Dungeon.Enemy.Mode prev_mode;
		public SAKURA.Consts.Gameplay.Dungeon.Enemy.Mode current_mode= Consts.Gameplay.Dungeon.Enemy.Mode.none;

		public SAKURA.Consts.Gameplay.Dungeon.Enemy.Sub_mode prev_sub_mode= Consts.Gameplay.Dungeon.Enemy.Sub_mode.none;
		public SAKURA.Consts.Gameplay.Dungeon.Enemy.Sub_mode current_sub_mode= Consts.Gameplay.Dungeon.Enemy.Sub_mode.none;


		public SAKURA.Consts.Gameplay.Dungeon.Enemy.Swing_mode swing_mode=Consts.Gameplay.Dungeon.Enemy.Swing_mode.none;

		public float rotation_y_swing_start=0;
		public float rotation_y_swing = 0;




		public SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point prev_move_point=null;
		public SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point current_move_point=null;
		public SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point next_move_point=null;

		public float distance_from_current_move_point;
		public float distance_between_current_and_next;

		public float rotation_y_target;
		public SAKURA.Consts.Gameplay.Dungeon.Enemy.Rotation_direction rotation_direction = SAKURA.Consts.Gameplay.Dungeon.Enemy.Rotation_direction.none;
		public float rotation_y_from_start;
		public float rotation_y_angle_between_start_and_target;

		public float next_walk_time;

	

		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {


			if(current_mode==Consts.Gameplay.Dungeon.Enemy.Mode.investigate){
				if(current_sub_mode==SAKURA.Consts.Gameplay.Dungeon.Enemy.Sub_mode.rotate){
				
					rotate ();
				
				}else if(current_sub_mode==SAKURA.Consts.Gameplay.Dungeon.Enemy.Sub_mode.swing){

					swing ();

				}else if(Time.time>next_walk_time){

				
					float rnd = Random.Range (0.0f,1.0f);

					if(rnd>enemy_settings.view_swing_prob){
						walk();
					}else{
						start_swing ();
					}

					next_walk_time=Time.time+enemy_settings.walk_turn_time;
				}
			}else if(current_mode==Consts.Gameplay.Dungeon.Enemy.Mode.caution_test){
				if(Time.time>next_walk_time){
					
					walk();
					
					next_walk_time=Time.time+enemy_settings.walk_turn_time;
				}
			
			}


		
		}

		public void initialize(Enemy_settings input_enemy_settings){

			enemy_settings = input_enemy_settings;

			enemy_object.set_angle_of_view (enemy_settings.angle_of_view);
			enemy_object.set_range_of_view (enemy_settings.range_of_view);

			change_enemy_mode (enemy_settings.initial_mode);
			change_enemy_sub_mode (enemy_settings.initial_sub_mode);

			variable_position = Gameplay.gameplay_static.variable_list_gameplay.create_variable<Variable_object_position> ("variable "+name+" position");

			variable_position.object_position_transform = enemy_object.position_transform;

			variable_rotation = Gameplay.gameplay_static.variable_list_gameplay.create_variable<Variable_object_rotation> ("variable "+name+" rotation");

			variable_rotation.object_rotation_transform = enemy_object.rotation_transform;

			initialize_enemy_point ();



		}

		public void initialize_enemy_point(){

			if(enemy_settings!=null){

				if(enemy_settings.initial_move_point!=null){

					set_local_position(enemy_settings.initial_move_point.transform.position);
					current_move_point = enemy_settings.initial_move_point;

					set_next_move_point();



				}else{
					print ("startpoint not set");
				}
			}else{
				print ("settings not set");
			}

		}

		public void destroy_enemy(){
				
		}

		public void start_swing(){

			change_enemy_sub_mode (Consts.Gameplay.Dungeon.Enemy.Sub_mode.swing);
			swing_mode = Consts.Gameplay.Dungeon.Enemy.Swing_mode.right;
			rotation_y_swing_start = get_local_rotation().eulerAngles.y;
			rotation_y_swing = 0.0f;

		}

		public void swing(){
				
			if(swing_mode==Consts.Gameplay.Dungeon.Enemy.Swing_mode.right){

				Vector3 temp= get_local_rotation().eulerAngles;


				rotation_y_swing-=enemy_settings.view_swing_speed_in_seconds* Time.deltaTime;
		
				
				Vector3 next_rotation_eular= new Vector3(temp.x,rotation_y_swing_start+rotation_y_swing,temp.z);
				set_local_rotation(Quaternion.Euler (next_rotation_eular));

				if(rotation_y_swing<-90.0f){
					swing_mode=Consts.Gameplay.Dungeon.Enemy.Swing_mode.left;
				}

			}else if(swing_mode==Consts.Gameplay.Dungeon.Enemy.Swing_mode.left){

				Vector3 temp= get_local_rotation().eulerAngles;
				
				
				rotation_y_swing+=enemy_settings.view_swing_speed_in_seconds* Time.deltaTime;
				
				
				Vector3 next_rotation_eular= new Vector3(temp.x,rotation_y_swing_start+rotation_y_swing,temp.z);
				set_local_rotation( Quaternion.Euler (next_rotation_eular));
				
				if(rotation_y_swing>90.0f){
					swing_mode=Consts.Gameplay.Dungeon.Enemy.Swing_mode.front;
				}

			}else if(swing_mode==Consts.Gameplay.Dungeon.Enemy.Swing_mode.front){

				Vector3 temp= get_local_rotation().eulerAngles;
				
				
				rotation_y_swing-=enemy_settings.view_swing_speed_in_seconds* Time.deltaTime;
				
				
				Vector3 next_rotation_eular= new Vector3(temp.x,rotation_y_swing_start+rotation_y_swing,temp.z);
				set_local_rotation(Quaternion.Euler (next_rotation_eular));
				
				if(rotation_y_swing<0){

					rotation_y_swing=0;

					temp= get_local_rotation().eulerAngles;

					
					
					next_rotation_eular= new Vector3(temp.x,rotation_y_swing_start,temp.z);

					set_local_rotation(Quaternion.Euler (next_rotation_eular));

					swing_mode=Consts.Gameplay.Dungeon.Enemy.Swing_mode.none;

					change_enemy_sub_mode(Consts.Gameplay.Dungeon.Enemy.Sub_mode.none);

					//change_enemy_mode_to_prev ();

				}
			}
		}

		public void walk(){

			//print (settings.walk_length_in_one_turn);

			if(current_move_point!=null&&next_move_point!=null){

				/*
				if(rotation_y_remaining!=0){



					if(Mathf.Abs(rotation_y_remaining)<settings.rotation_in_one_turn){
					
						rotation.rotation=	Quaternion.Euler(new Vector3(0,rotation_y_target,0));
						rotation_y_remaining=0;
					
					}else{

						rotation.Rotate( new Vector3(0,Mathf.Sign( rotation_y_remaining )*settings.rotation_in_one_turn,0));
						rotation_y_remaining=Mathf.Sign( rotation_y_remaining )*(Mathf.Abs(rotation_y_remaining) - settings.rotation_in_one_turn);
					}
					
				 */
					distance_from_current_move_point+=enemy_settings.walk_length_in_one_turn;


					if(distance_from_current_move_point>= distance_between_current_and_next){


						set_next_move_point();
					
					}else{
						Vector3 temp=weighted_average_v3(current_move_point.transform.position,next_move_point.transform.position,distance_from_current_move_point);

						set_local_position(temp);
					}


			}else{
				//initalize_enemy_point();

				print ("current or/and next point not set");
			}


		}

		public void start_rotation(float input_rotation_y_target){

			change_enemy_sub_mode (Consts.Gameplay.Dungeon.Enemy.Sub_mode.rotate);
			rotation_y_target = Sakura.normalize_angle(input_rotation_y_target);

			float current_rotation_y =Sakura.normalize_angle(get_local_rotation().eulerAngles.y);

			rotation_y_from_start = 0;


			float greater_angle = 0;
			float smaller_angle = 0;


			if(rotation_y_target>current_rotation_y){
				greater_angle=rotation_y_target;
				smaller_angle=current_rotation_y;
				rotation_direction=Consts.Gameplay.Dungeon.Enemy.Rotation_direction.left;
			}else{

				greater_angle=current_rotation_y;
				smaller_angle=rotation_y_target;
				rotation_direction=Consts.Gameplay.Dungeon.Enemy.Rotation_direction.right;
			}

			if(greater_angle-smaller_angle<180){
				rotation_y_angle_between_start_and_target= greater_angle-smaller_angle;

			}else{


				rotation_direction=flip_rotation(rotation_direction);
	
				rotation_y_angle_between_start_and_target= 360.0f -(greater_angle-smaller_angle);
			}

		

		}

		public SAKURA.Consts.Gameplay.Dungeon.Enemy.Rotation_direction flip_rotation(SAKURA.Consts.Gameplay.Dungeon.Enemy.Rotation_direction input_rotation){

			SAKURA.Consts.Gameplay.Dungeon.Enemy.Rotation_direction output=Consts.Gameplay.Dungeon.Enemy.Rotation_direction.none;

			if(input_rotation==Consts.Gameplay.Dungeon.Enemy.Rotation_direction.left){
				output=Consts.Gameplay.Dungeon.Enemy.Rotation_direction.right;
			}else if(input_rotation==Consts.Gameplay.Dungeon.Enemy.Rotation_direction.right){
				output=Consts.Gameplay.Dungeon.Enemy.Rotation_direction.left;
			}

			return output;
			
		}

		public float rotation_to_sign(SAKURA.Consts.Gameplay.Dungeon.Enemy.Rotation_direction input_rotation){
			
			float output = 0;
			
			if(input_rotation==Consts.Gameplay.Dungeon.Enemy.Rotation_direction.left){
				output= 1;
			}else if(input_rotation==Consts.Gameplay.Dungeon.Enemy.Rotation_direction.right){
				output= -1;
			}
			
			return output;
			
		}



		public void rotate(){


			Vector3 current_rotation_euler= get_local_rotation().eulerAngles;

			float additional_rotation_y=rotation_to_sign (rotation_direction)*enemy_settings.rotation_speed_in_seconds* Time.deltaTime;

			Vector3 next_rotation_euler = current_rotation_euler+ new  Vector3 (0,additional_rotation_y,0);

			rotation_y_from_start+=enemy_settings.rotation_speed_in_seconds* Time.deltaTime;

			if(rotation_direction==Consts.Gameplay.Dungeon.Enemy.Rotation_direction.left 
			   ||rotation_direction==Consts.Gameplay.Dungeon.Enemy.Rotation_direction.right){

				if(rotation_y_from_start<rotation_y_angle_between_start_and_target){
					
					set_local_rotation(Quaternion.Euler (next_rotation_euler));

				}else{

					set_local_rotation(Quaternion.Euler ( new Vector3(current_rotation_euler.x,rotation_y_target, current_rotation_euler.z)));

					change_enemy_sub_mode(Consts.Gameplay.Dungeon.Enemy.Sub_mode.none);
				}
			}

		}


		public Vector3 weighted_average_v3(Vector3 from, Vector3 to, float length_from){
			Vector3 output = from;
			float length_between = (to - from).magnitude;
			
			
			
			if(length_between!=0){
				
				output= from*((length_between-length_from)/length_between)+to*(length_from/length_between);
			}
			
			
			
			return output;
		}

	

		public void set_next_move_point(){
			if(current_move_point!=null){



				if(next_move_point!=null){

					prev_move_point=current_move_point;
					current_move_point=next_move_point;

				}

				if(current_move_point.connected_points.Count>0){

					SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point temp=  current_move_point.connected_points[0];



					if(current_mode==Consts.Gameplay.Dungeon.Enemy.Mode.investigate){
						temp=pick_random_next_move_point();
					}else if(current_mode==Consts.Gameplay.Dungeon.Enemy.Mode.caution_test){
						temp=pick_closest_point_from_player();
					}

					next_move_point=temp;

					distance_from_current_move_point=0;
					distance_between_current_and_next= (current_move_point.transform.position-next_move_point.transform.position).magnitude;

					float temp_target= Sakura.from_to_rotation_y(new Vector3(0,0,1),next_move_point.transform.position-current_move_point.transform.position).eulerAngles.y;

					start_rotation(temp_target);


					//rota

					//float current_rotation= rotation.rotation.eulerAngles.y;


					/*
					rotation_y_remaining =rotation_y_target-current_rotation;

					if(rotation_y_remaining>=180){
						rotation_y_remaining-=360.0f;
					}else if(rotation_y_remaining<=-180){
						rotation_y_remaining+=360.0f;
					}*/


					//rotation_y_remaining=	;

				}else{
					print ("current point is not connected to any point!");
				}
				
					

			}else{
				print ("currentpoint not set");
			}
		}

		public void change_enemy_mode(SAKURA.Consts.Gameplay.Dungeon.Enemy.Mode input_mode){
			prev_mode = current_mode;
			current_mode = input_mode;
		}

		public void change_enemy_mode_to_prev(){
			current_mode = prev_mode;
			prev_mode = current_mode;
		}

		public void change_enemy_sub_mode(SAKURA.Consts.Gameplay.Dungeon.Enemy.Sub_mode input_sub_mode){
			prev_sub_mode = current_sub_mode;
			current_sub_mode = input_sub_mode;
		}


		public SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point pick_random_next_move_point(){


			SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point output = null;


			if(current_move_point.connected_points.Count==0){

				print ("current point has no connected point!");
			}else if(current_move_point.connected_points.Count==1){

				output= current_move_point.connected_points[0];

			}else if(current_move_point.connected_points.Count>=2){

				List<int> slot = new List<int>();



				int counter=0;


				foreach(SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point point in current_move_point.connected_points){

					bool flag= true;

					if(prev_move_point!=null){
						if(point.id==prev_move_point.id){
							flag=false;
						
						}
					}

					if(flag){
						slot.Add (counter);
					}

					counter++;
				}

				if(slot.Count>0){

					int picked=Random.Range(0,slot.Count);
				
					output= current_move_point.connected_points[slot[picked]];

					print ("connected_points: " +counter+" slot: "+slot.Count +" picked: "+picked +" id: "+output.id);
				}else{

					output= current_move_point.connected_points[0];

					print("connected points are duplicated!");
				}

			
			}





			return output;
		}

		public SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point pick_closest_point_from_player(){

			SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point output = null;

			SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point current= dungeon.player.get_current_point();

			if(current!=null){
				output=current_move_point.get_closest_point_from_connected(current);
			}else{
				print ("closest point not found");

				output=pick_random_next_move_point();
			}


			return output;
		}



		public void set_local_position(Vector3 input_position){
			variable_position.change_object_local_position (input_position);
		}

		public Vector3 get_local_position(){
			return 	variable_position.get_object_local_position();
		}

		public Vector3 get_global_position(){
			return variable_position.get_object_global_position ();
		}

		public void set_local_rotation(Quaternion input_rotation){
			variable_rotation.change_object_local_rotation (input_rotation);
		}
		
		public Quaternion get_local_rotation(){
			return variable_rotation.get_object_local_rotation ();
		}
	


	}
}
