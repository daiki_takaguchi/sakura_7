﻿using UnityEngine;
using System.Collections;


namespace SAKURA.AUDIO{
	public class Bgm : MonoBehaviour {

		public AudioSource audio;

		public enum Audio_state{
			play,
			stop
		}


		public bool mute;


		public Audio_state state;

		void Start () {

		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void play(AudioClip input_BGM){

		
			if (state == Audio_state.play) {
			
				audio.Stop();
			}

			if(!mute){

				if (input_BGM != null) {
					state = Audio_state.play;
					audio.clip = input_BGM;
					
					audio.Play ();
				}else{
					Debug.LogError("BGM not found!");
				}
			}
			

		}

		public void stop(){
			state = Audio_state.stop;
			audio.Stop ();
		}




	}
}
