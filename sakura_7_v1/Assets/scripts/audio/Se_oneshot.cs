﻿using UnityEngine;
using System.Collections;


namespace SAKURA.AUDIO{
	public class Se_oneshot : MonoBehaviour {

		public AudioSource audio;

		//public AudioSource beat;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void play(AudioClip input_SE){
			audio.clip = input_SE;
			audio.Play ();

		}

		public void change_pan(float input_pan){
			audio.panStereo = input_pan;
			//print ("Pan:"+input_pan);
		}

		public void change_volume(float input_volume){
			audio.volume=input_volume;
			//print ("Pan:"+input_volume);
		}
	}
}
