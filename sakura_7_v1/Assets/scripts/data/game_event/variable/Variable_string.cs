﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_string : Variable {


		public string initial_value;
		public string current_value;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame


		public override void initialize(){
			current_value = initial_value;
		}

		public override void save(string input_prefix){
			print ("saving "+input_prefix+variable_name+": value "+current_value);
			PlayerPrefs.SetString (input_prefix+variable_name,current_value);
		}
		
		public override void load(string input_prefix){
			
			current_value=PlayerPrefs.GetString (input_prefix+variable_name);
			print ("loading "+input_prefix+variable_name+": value "+current_value);
		}

	}
}
