﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_float : Variable {

		
		
		//public string data_name;

		public float initial_value;
		public float current_value;



		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame

		public override void initialize(){
			current_value = initial_value;
		}

		public override void save(string input_prefix){
			print ("saving "+input_prefix+variable_name+": value "+current_value);
			PlayerPrefs.SetFloat (input_prefix+variable_name,current_value);
		}
		
		public override void load(string input_prefix){

			current_value=PlayerPrefs.GetFloat (input_prefix+variable_name);
			print ("loading "+input_prefix+variable_name+": value "+current_value);
		}

	}
}
