﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_int : Variable {

		
		
		//public string data_name;

		public int initial_value;
		public int current_value;



		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame

		public override void initialize(){
			current_value = initial_value;
		}

		public override void save(string input_prefix){
			print ("saving "+input_prefix+variable_name+": value "+current_value);
			PlayerPrefs.SetInt (input_prefix+variable_name,current_value);
		}
		
		public override void load(string input_prefix){

			current_value=PlayerPrefs.GetInt (input_prefix+variable_name);
			print ("loading "+input_prefix+variable_name+": value "+current_value);
		}

	}
}
