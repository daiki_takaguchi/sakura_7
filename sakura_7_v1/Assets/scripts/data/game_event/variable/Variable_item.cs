﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_item : Variable {

		public GameObject item_object;

		public Consts.Data.Game_event.Variable.Variable_item_state initial_state;
		public Consts.Data.Game_event.Variable.Variable_item_state current_state;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame


		public override void initialize(){
			current_state = initial_state;
		}

		public void change_state( Consts.Data.Game_event.Variable.Variable_item_state input_state  ){

			if(input_state==Consts.Data.Game_event.Variable.Variable_item_state.unavailable){

			}else if(input_state==Consts.Data.Game_event.Variable.Variable_item_state.not_found){
				item_object.SetActive(true);

			}else if(input_state==Consts.Data.Game_event.Variable.Variable_item_state.owned){
				item_object.SetActive(false);
			}
		}

		
		public override void save(string input_prefix){
			print ("saving "+input_prefix+variable_name+": value "+current_state);
			PlayerPrefs.SetInt (input_prefix+variable_name,(int) current_state);
		}
		
		public override void load(string input_prefix){
			
			current_state=(Consts.Data.Game_event.Variable.Variable_item_state) PlayerPrefs.GetInt (input_prefix+variable_name);
			print ("loading "+input_prefix+variable_name+": value "+current_state);
		}

	}
}
