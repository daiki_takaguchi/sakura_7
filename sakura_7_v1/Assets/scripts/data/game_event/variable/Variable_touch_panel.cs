﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.ACTION;


namespace SAKURA.DATA.GAME_EVENT.VARIABLE{

	[ExecuteInEditMode]
	public class Variable_touch_panel: Variable {

		//private Consts.Ui.Objects.Touch_panel_state prev_state;
		public Consts.Ui.Objects.Touch_panel_state state;



		// Use this for initialization
		void Start () {
		
		}
		


		public void change_value(Consts.Ui.Objects.Touch_panel_state input_state){

			if(state!=input_state){
				//prev_state=state;
				print (input_state);
				state = input_state;
				waiting_game_event_list.initalize_action_pointer ();
				waiting_game_event_list.run_events ();

			}
		}

		public override void initialize(){

		}

		public void set_condition(){

		}

	}
}
