﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_vector_3 : Variable {

		
		
		//public string data_name;

		public Vector3 initial_value;
		public Vector3 current_value;



		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame

		public override void initialize(){
			current_value = initial_value;
		}

		public override void save(string input_prefix){
			print ("saving "+input_prefix+variable_name+": value "+current_value);


			PlayerPrefs.SetFloat (input_prefix+variable_name+"_x",current_value.x);
			PlayerPrefs.SetFloat (input_prefix+variable_name+"_y",current_value.y);
			PlayerPrefs.SetFloat (input_prefix+variable_name+"_z",current_value.z);
		}
		
		public override void load(string input_prefix){

			float x=PlayerPrefs.GetFloat (input_prefix+variable_name+"_x");
			float y=PlayerPrefs.GetFloat (input_prefix+variable_name+"_y");
			float z=PlayerPrefs.GetFloat (input_prefix+variable_name+"_z");

			current_value = new Vector3 (x,y,z);

			print ("loading "+input_prefix+variable_name+": value "+current_value);
		}

	}
}
