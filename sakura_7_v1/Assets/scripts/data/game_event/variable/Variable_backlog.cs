﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.ACTION;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	[ExecuteInEditMode]
	public class Variable_backlog : Variable {


		public static Variable_backlog variable_backlog_static;

		public List<Action_person_image> action_person_image_list;
		public List<Action_adv_text> action_adv_text_list;

		public List<string> backlog=new List<string>();
		public int backlog_pointer;

		// Use this for initialization
		void Start () {
			if(variable_backlog_static==null){
				variable_backlog_static=this;
			}
		}

		void Update () {
			if(!Application.isPlaying){

				action_person_image_list= new List<Action_person_image>(FindObjectsOfType<Action_person_image>());

				action_adv_text_list= new List<Action_adv_text>(FindObjectsOfType<Action_adv_text>());

				set_prefix();

			}
		}

		public void set_prefix(){

			string prefix="system_backlog_text_";

			int count = 0;

			foreach(Action_person_image one in action_person_image_list){

				one.save_data_prefix=prefix+count;

				count++;
			}


			foreach(Action_adv_text one in action_adv_text_list){

				one.save_data_prefix=prefix+count;

				count++;
			}



		}

		public override void save(string input_prefix){



		}
		
		public override void load(string input_prefix){

			print ("loading backlog");
			
			load_backlog ();
		}

		public void add_backlog(string input_string){

		}

		public void load_backlog(){

			string prefix="system_backlog_backlog_";

			//print (Data.data_static.game_settings.max_backlog_count);
			
			backlog = new List<string> ( new string[Data.data_static.game_settings.max_backlog_count]);

			//print (backlog.Count);

			for(int i=0;i<Data.data_static.game_settings.max_backlog_count;i++){

				string data_key=prefix+i;

				if(PlayerPrefs.HasKey(data_key)){
					backlog[i]=PlayerPrefs.GetString(data_key);
				}
			}

			string key = ("system_backlog_backlog_pointer");

			if (PlayerPrefs.HasKey (key)) {
				backlog_pointer = PlayerPrefs.GetInt (key);
			} else {
				backlog_pointer = 0;
			}


			
		}


		public void save_backlog(){
			/*
			string prefix="system_backlog_backlog_";

			string data_key=prefix+backlog_pointer;

			PlayerPrefs.SetString(data_key,backlog[backlog_pointer]);
			
			increment_backlog_pointer ();*/
		}

		public List<string> get_backlog(){

			List<string> output = new List<string> ();

			int temp = prev_backlog_pointer ();

			for (int i=temp; i<Data.data_static.game_settings.max_backlog_count;i++){
				output.Add(backlog[i]);
			}

			for (int i=0; i<temp;i++){
				output.Add(backlog[i]);
			}

			return output;
		}

		public void increment_backlog_pointer(){
		
			backlog_pointer++;

			if(backlog_pointer>=Data.data_static.game_settings.max_backlog_count){
				backlog_pointer=0;
			}

		
		}

		public int prev_backlog_pointer(){
			
			int output = backlog_pointer - 1;

			if (output == -1) {
				output=Data.data_static.game_settings.max_backlog_count;
			};
			
			return output;
		}



		
		// Update is called once per frame


		public override void initialize(){
			//current_value = initial_value;
		}

		public void player_read_text(Action_adv_text input_action_adv_text){
		

			input_action_adv_text.save ();

			backlog [backlog_pointer] = input_action_adv_text.text;

			string prefix="system_backlog_backlog_";
			
			string data_key=prefix+backlog_pointer;


			
			PlayerPrefs.SetString(data_key,backlog[backlog_pointer]);
			
			increment_backlog_pointer ();

			string key = ("system_backlog_backlog_pointer");

			PlayerPrefs.SetInt(key,backlog_pointer);

			PlayerPrefs.Save ();

		}



	}
}
