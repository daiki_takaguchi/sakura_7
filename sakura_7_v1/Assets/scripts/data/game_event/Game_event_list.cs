﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.DATA.SCENE_SETTINGS;

namespace SAKURA.DATA{
	[ExecuteInEditMode]
	public class Game_event_list : MonoBehaviour {

		public List<Game_event> game_event_list =new List<Game_event>();

		public Variable_list variable_list;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

			if(!Application.isPlaying){
				refresh_children();

			}
		
		}

		public void refresh(Scene_sequence_state input_state){

			if(input_state.variable_list!=null){
				variable_list=input_state.variable_list;
			}

			//refresh_children ();
		}

		public void refresh_children(){
			game_event_list= Sakura.get_monos_from_children<Game_event>(transform);
			
			int event_no=0;
			
			foreach(Game_event game_event in game_event_list){

				game_event.name= "event "+event_no;
				
				game_event.refresh_children(this);


				/*game_event.condition_list=Sakura.get_mono_from_children<Condition_list>(game_event.transform);
				
				game_event.condition_list.name="condition_list";
				
				game_event.condition_list.condition_list=Sakura.get_monos_from_children<Condition>(game_event.condition_list.transform);
				
				int condition_no=0;
				
				foreach(Condition condition in game_event.condition_list.condition_list){
					
					
					condition.name="conditon "+condition_no+": "+condition.condition_type.ToString();
					
					condition_no++;
				}
				
				game_event.action_list=Sakura.get_mono_from_children<Action_list>(game_event.transform);
				
				game_event.action_list.name="action_list";
				
				game_event.action_list.action_list=Sakura.get_monos_from_children<Action>(game_event.action_list.transform);
				
				int action_no=0;
				
				foreach(Action action in game_event.action_list.action_list){
					
					
					action.name="action "+action_no+": "+action.action_type.ToString();
					
					action_no++;
				}*/
				
				
				
				
				
				event_no++;
			}

		}

		public Game_event add_game_event(Game_event input_game_event){
			game_event_list.Add (input_game_event);

			return input_game_event;
		}

		public Game_event create_game_event(){

			GameObject temp = new GameObject ();
			temp.transform.parent = transform;

			Game_event output = temp.AddComponent<Game_event> ();
			output.parent_game_event_list = this;
			game_event_list.Add (output);
			refresh_children ();

			output.initialize ();

			return output;
		}

		public void remove_game_event(Game_event input_game_event){

			game_event_list.Remove (input_game_event);
			Destroy(input_game_event.gameObject);

		}

		public void destroy_all_game_events(){
			
			List<Game_event> temp = new List<Game_event> (game_event_list);
			//print (name);
			foreach(Game_event game_event in temp){

				DestroyImmediate(game_event.gameObject);

			}

			game_event_list = new List<Game_event> ();
			
		}

		public List<Action_list> get_available_action_lists(){
			return null;
		}

		public bool run_events(){

			bool is_finished = false;

			List<Game_event> temp = new List<Game_event> (game_event_list);

			foreach(Game_event game_event in temp){

				if(game_event!=null){
					game_event.run_event();
				}else{

				}

			}

			return is_finished;
		}

		public void initalize_action_pointer(){
			List<Game_event> temp = new List<Game_event> (game_event_list);
			
			foreach(Game_event game_event in temp){
				game_event.initalize_action_pointer();
			}
		}

		/*
		public void variable_changed(Variable input_variable){
			foreach(Game_event game_event in game_event_list){
				//game_event.variable_changed
			}
		}*/


	}
}
