﻿using UnityEngine;
using System.Collections;
using SAKURA.DATA.GAME_EVENT.VARIABLE;


namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Change_variable_item : Action {

		public Variable_item variable_item;
		public Consts.Data.Game_event.Variable.Variable_item_state variable_item_state;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public override void run(Action_list input_action_list){
			
			run ();
			
		}
		
		
		public override void run(){
			
			variable_item.change_state (variable_item_state);
			
		}
	}
}
