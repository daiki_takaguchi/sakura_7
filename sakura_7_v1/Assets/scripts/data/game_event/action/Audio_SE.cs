﻿using UnityEngine;
using System.Collections;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Audio_SE : Action {


		public Consts.Data.Game_event.Action.Audio_operation audio_oparation;
		
		public string SE_name;
			
		//public float fadeout_time=0.0f;



		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		
		public override void run(Action_list input_action_list){
			switch (audio_oparation){
			case Consts.Data.Game_event.Action.Audio_operation.play:
				
				string file=Consts.Audio.source_path_SE + SE_name.ToString();
				AudioClip temp =Resources.Load<AudioClip> (file);
				
				print (file);
				
				Sakura.sakura_static.audio.se.play(temp);
				
				break;
				
				
			case Consts.Data.Game_event.Action.Audio_operation.stop:
				
				//Sakura.sakura_static.audio.se.stop();
				
				break;
				
			}
			
			
			
			input_action_list.run_next ();
			
		}
	}
}
