﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.UI.LAYER;

namespace SAKURA.DATA.GAME_EVENT.ACTION{

	public class Action_adv_text : Action {


		public SAKURA.Consts.Data.Game_event.Action.Person_name person_name;

		public string person_name_string;

		public string text;

		public string remaining_text;

		public SAKURA.Consts.Data.Game_event.Action.Text_state state=Consts.Data.Game_event.Action.Text_state.waiting_for_run;

		public Consts.Data.Game_event.Variable.Variable_text_state variable_text_state;

		public bool is_loaded = false;

		[HideInInspector] public SAKURA.Consts.Ui.Objects.Touch_panel_state touch_panel_state;


		[HideInInspector] public bool is_long_pressed=false;

		[HideInInspector] public bool is_tapped=false;

		[HideInInspector] public bool is_run_finished=false;

		[HideInInspector] private Coroutine waiting_croutine;

		public string save_data_prefix;


		// Use this for initialization
		void Start () {
		
		}
		



		IEnumerator run_again(float wait_time) {
			yield return new WaitForSeconds(wait_time);
			run_loop (true);
		}


		public override void run(Action input_action){

			load ();

			parent_action = input_action;



			run_loop (false);


		}

		public override void run(Action_list input_action_list){

			load ();

			parent_action_list = input_action_list;

			run_loop (false);

		}

		public void run_loop(bool is_loop){



			//Variable_touch_panel variable_touch_panel= (Variable_touch_panel) Gameplay.gameplay_static.get_variable(typeof(Variable_touch_panel));
			
			Adv_text_layer adv_text = (Adv_text_layer) Ui.ui_static.layer_list.get_layer (Consts.Ui.Layer_type.adv_text);

			if (state == Consts.Data.Game_event.Action.Text_state.waiting_for_run) {
				


				notify_me_in_condition(Consts.Ui.Objects.Touch_panel_state.two_finger_long_press);

				notify_me_in_condition(Consts.Ui.Objects.Touch_panel_state.one_finger_tap);

			

				SAKURA.Ui.ui_static.show_if_hidden (Consts.Ui.Layer_type.adv_text);

				if(person_name_string!=""){
					adv_text.set_name(person_name_string);
				}else {
					adv_text.set_name(person_name);
				}


				Variable_float text_speed_variable=(Variable_float) SAKURA.Data.data_static.save_data.system_data.get_variable("text_speed");

				float text_speed=0.0f;

				if(text_speed_variable!=null){

					text_speed=text_speed_variable.current_value;
				}
				
				if(text_speed<=0){
					
					adv_text.main_text.set_text (text,Consts.Ui.Objects.Text_mode.all_at_once,out remaining_text);
					
					state=Consts.Data.Game_event.Action.Text_state.waiting_for_next_button;
					
				}else{
					if(adv_text.main_text.set_text (text,Consts.Ui.Objects.Text_mode.type_writer,out remaining_text)){
						
						state=Consts.Data.Game_event.Action.Text_state.waiting_for_next_button;
					}else{
						waiting_croutine= StartCoroutine(run_again(text_speed));
						state=Consts.Data.Game_event.Action.Text_state.showing;
					}
					
					
				}
				
			}else if(state==Consts.Data.Game_event.Action.Text_state.showing){
				
				if(touch_panel_state==Consts.Ui.Objects.Touch_panel_state.none){
					if(adv_text.main_text.add_text (remaining_text,Consts.Ui.Objects.Text_mode.type_writer,out remaining_text)){

						state=Consts.Data.Game_event.Action.Text_state.waiting_for_next_button;
						
					}else{
						Variable_float text_speed_variable=(Variable_float) SAKURA.Data.data_static.save_data.system_data.get_variable("text_speed");
						
						float text_speed=text_speed_variable.current_value;

						waiting_croutine =StartCoroutine(run_again(text_speed));
					};
					
				}else if(touch_panel_state==Consts.Ui.Objects.Touch_panel_state.one_finger_tap){


					adv_text.main_text.add_text (remaining_text,Consts.Ui.Objects.Text_mode.all_at_once,out remaining_text);

					state=Consts.Data.Game_event.Action.Text_state.waiting_for_next_button;
					
					touch_panel_state=Consts.Ui.Objects.Touch_panel_state.none;
					
				}else if(touch_panel_state==Consts.Ui.Objects.Touch_panel_state.one_finger_long_press){
					adv_text.main_text.add_text (remaining_text,Consts.Ui.Objects.Text_mode.all_at_once,out remaining_text);
					
					state=Consts.Data.Game_event.Action.Text_state.waiting_for_next_button;
					
					is_long_pressed=true;

					waiting_croutine =StartCoroutine(run_again(0.5f));
				}
				
				
				
				
			}else if(state==Consts.Data.Game_event.Action.Text_state.waiting_for_next_button){

				print ("waiting");
				
				if(touch_panel_state==Consts.Ui.Objects.Touch_panel_state.one_finger_tap
				   ||is_long_pressed){
					is_run_finished=true;


				}
			}
			
			if(is_run_finished){
					
				print ("text finished");

					

					created_game_event_list.destroy_all_game_events();
					
					Destroy(created_game_event_list);
					
					if(parent_action!=null){
						
					/*
						if(parent_action.GetType ()==typeof(Action_person_image)){
							Action_person_image temp= (Action_person_image) parent_action;
							
						save_data_prefix=temp.save_data_prefix;
						}*/

						
						
						parent_action.child_action_finished(this);
						Destroy(this.gameObject);

						
						
						
					}else{

					}

					player_read_text();
					
					if(parent_action_list!=null){
						parent_action_list.run_next();
					}



				state=Consts.Data.Game_event.Action.Text_state.waiting_for_run;
			
			}

		}

		public void initialize(SAKURA.Consts.Data.Game_event.Action.Person_name input_person_name,string input_text){
			person_name = input_person_name;
			text = input_text;
		
		}

		public void initialize(string input_person_name,string input_text){
			person_name_string = input_person_name;
			text = input_text;
			
		}


		public override void variable_changed (Variable input_variable,Game_event input_game_event)
		{

	

			if(input_variable is Variable_touch_panel){


				Variable_touch_panel temp= (Variable_touch_panel) input_variable ;


				if(temp.state==Consts.Ui.Objects.Touch_panel_state.one_finger_long_press
				   ||temp.state==Consts.Ui.Objects.Touch_panel_state.one_finger_tap){

					print (state+" "+temp.state);

					touch_panel_state=temp.state;


					if(waiting_croutine!=null){

						StopCoroutine(waiting_croutine);
					}

					run_loop(true);
				}
			}
		}


		private void notify_me_in_condition(Consts.Ui.Objects.Touch_panel_state input_condition_state){
			if(created_game_event_list==null){
				created_game_event_list=gameObject.AddComponent<Game_event_list>();
			}


			Game_event game_event =created_game_event_list.create_game_event();
			
			Condition_touch_panel condition =(Condition_touch_panel) game_event.add_condition<Condition_touch_panel>();
			
			condition.condition_touch_panel_state=input_condition_state;
			
			Notify_variable_changed notify =(Notify_variable_changed) game_event.add_action<Notify_variable_changed>();
			
			notify.initialize(this,Gameplay.gameplay_static.get_variable (typeof(Variable_touch_panel)));
			
			game_event.add_event_to_variables();

			print (created_game_event_list.game_event_list.Count);



		}

		public void player_read_text(){
			variable_text_state = Consts.Data.Game_event.Variable.Variable_text_state.read;
			Variable_backlog.variable_backlog_static.player_read_text (this);
		}

		public void save(){

			if(save_data_prefix!=""){

				PlayerPrefs.SetInt (save_data_prefix, (int)variable_text_state);
				//PlayerPrefs.Save ();
			}
		}
		
		public void load(){

			if(!is_loaded){

				if (save_data_prefix != "") {
					if(PlayerPrefs.HasKey(save_data_prefix)){
						variable_text_state = (Consts.Data.Game_event.Variable.Variable_text_state)PlayerPrefs.GetInt (save_data_prefix);
					}
				}

				is_loaded=true;

			}
		}




	

	}



}
