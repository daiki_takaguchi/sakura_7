﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.GAME_EVENT.ACTION{

	public class Notify_variable_changed : Action {

		/*
		public override SAKURA.Consts.Data.Scene_settings.Action.Action_type action_type
		{
			get { return Consts.Data.Scene_settings.Action.Action_type.change_adv_text;}
		}
		*/

		public Variable variable_to_watch;
		public Action action_to_notify;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

		}

		public void initialize(Action input_action_to_notify, Variable input_variabel_to_watch){
			action_to_notify = input_action_to_notify;
			variable_to_watch = input_variabel_to_watch;
		}




		public override void run(Action_list input_action_list){

			//print (variable_to_watch);
			action_to_notify.variable_changed (variable_to_watch,input_action_list.parent_game_event);

			input_action_list.run_next ();
	
		}

	




		/*
		public override void transfer_current_touch_panel_state(Consts.Ui.Objects.Touch_panel_state input_touch_panel_state){

			current_touch_panel_state = input_touch_panel_state;

			if(input_touch_panel_state==Consts.Ui.Objects.Touch_panel_state.one_finger_tap){
				is_tapped=true;
			}

		}*/

	}



}
