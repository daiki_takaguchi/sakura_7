﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.GAME_EVENT.ACTION{

	[ExecuteInEditMode]
	public class Action : MonoBehaviour {

		public string action_tag="";

		[HideInInspector] public string prev_action_tag="";
		public int action_list_order;
		[HideInInspector] public Action_list parent_action_list;
		[HideInInspector] public Action parent_action;
		[HideInInspector] public Action child_action;
		[HideInInspector] public Game_event_list created_game_event_list=null;

		[HideInInspector] public bool destroy_called_by_parent=false;
	

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if(!Application.isPlaying){

				if(action_tag!=prev_action_tag){

					Data data= FindObjectOfType<Data>();

					if(prev_action_tag!=""){
						data.jump_tag_list.remove_data(this);
					}


					data.jump_tag_list.set_data(this);
					prev_action_tag=action_tag;

				}
			}
		
		}

		
		public virtual void parse(string input_string){
			//if(action_type==Consts.Data.Scene_settings.Action.Action_type.Jump){
			//Ui.jump(jump_layer_type);
			//}
			
			//action_calling = input_action;
			
			//return true;
		}


		public virtual void run(){

		}

		public virtual void run(Action input_action){
			//if(action_type==Consts.Data.Scene_settings.Action.Action_type.Jump){
			//Ui.jump(jump_layer_type);
			//}

			//action_calling = input_action;

			//return true;
		}

		public virtual void run(Action_list input_action_list){
			//if(action_type==Consts.Data.Scene_settings.Action.Action_type.Jump){
				//Ui.jump(jump_layer_type);
			//}
			/*
			if(action_calling!=null){
				action_calling.run_finished(this);
				action_calling=null;
			}*/

			//return true;
		}

		public virtual bool save(string file_name){
			bool output = true;

			return output;
		}

		public virtual bool load(string file_name){
			bool output = true;
			
			return output;
		}

		public virtual void variable_changed(Variable input_variable,Game_event input_game_event){
				
		}

		public virtual void child_action_finished(Action input_child_action){

		}

		void OnDestroy(){
			if (destroy_called_by_parent) {
				//print ("parent_destroyed");
			} else {
				Jump_tag_list temp=FindObjectOfType<Jump_tag_list>();

				if(temp!=null){
					temp.refresh();
				}

			}
		}

		/*
		public void set_variable_list(List<string> input_variable_name,List<SAKURA.Consts.Unity_editor.Csv_to_action_conversion.Variable_type> input_type, List<string> input_variable_value){


			for (int i=0;i<input_variable_name.Count;i++) {
				FieldInfo info = this.GetType ().GetField (input_variable_name[i]);

				switch(input_type[i]){
					case SAKURA.Consts.Unity_editor.Json_to_action_conversion.Variable_type.string_type:


					info.SetValue(this,input_variable_value[i]);
					break;
				
				}
			}

		}*/

	}
}
