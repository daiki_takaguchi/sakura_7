﻿using UnityEngine;
using System.Collections;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Audio_BGM : Action {

		public Consts.Data.Game_event.Action.Audio_operation audio_operation;

		public string BGM_name;


		//public string
		
		

		// Use this for initialization
		void Start () {
		
		}
		

		
		public override void run(Action_list input_action_list){

			switch (audio_operation){
				case Consts.Data.Game_event.Action.Audio_operation.play:

					string file=Consts.Audio.source_path_BGM + BGM_name.ToString();
					AudioClip temp =Resources.Load<AudioClip> (file);

					print (file);
					
					Sakura.sakura_static.audio.bgm.play(temp);
					
					break;


				case Consts.Data.Game_event.Action.Audio_operation.stop:

					Sakura.sakura_static.audio.bgm.stop();
				
				break;

			}



			input_action_list.run_next ();

		}
	}
}
