﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.UNITY_EDITOR;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.VARIABLE;


namespace SAKURA.DATA.GAME_EVENT.ACTION{

	[ExecuteInEditMode]
	public class Action_list : MonoBehaviour {

		// Use this for initialization

		public Transform list;

		public bool insert_action = false;

		public int insert_at;

		public Game_event parent_game_event;

		public bool save_data_now;
		public bool load_data_now;
		public bool load_data_confirm;

		public string json_data_folder;
		public string json_data_name="action_list.json";

	//	public string data_file;

		public List<Action> action_list= new List<Action>();

		//public Game_event 

		public Variable_int current_action_pointer;

		[HideInInspector] public bool is_refresh_called_by_game_event=false;

		[HideInInspector] public bool is_refresh_called_by_self=false;


		//private int max_loop_count=1000;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {



			if(!Application.isPlaying){
				if(save_data_now){


					save();

					save_data_now=false;
				}

				if(load_data_now&&load_data_confirm){
					load();
					load_data_now=false;
					load_data_confirm=false;

				}

				if(current_action_pointer==null){

					initalize_pointer();
				}

				if(insert_action){

					int counter=0;

					foreach(Action temp in action_list){

						temp.transform.parent=null;

						if(counter==insert_at){
							GameObject a= new GameObject();
							a.AddComponent<Action>();
							a.transform.parent=list;
						}

						temp.transform.parent=list;

						counter++;

					}

					if(counter==insert_at){
						GameObject a= new GameObject();
						a.AddComponent<Action>();
						a.transform.parent=list;
					}


					insert_action=false;
				}


				is_refresh_called_by_self=true;
				refresh_children();
				//refresh_children();
			}
		
		}



		public void save(){

			
			//foreach(Action action in action_list){
				
			Game_settings game_settings= GameObject.FindObjectOfType<Game_settings>();


			if(game_settings!=null){

				game_settings.json_to_action_conversion_list.save_data(json_data_folder+json_data_name,action_list);

			}
			//}
		
		}


		public void load(){




			clear_list ();


			Game_settings game_settings= GameObject.FindObjectOfType<Game_settings>();

			action_list = new List<Action>();
			
			if(game_settings!=null){
				
				game_settings.json_to_action_conversion_list.load_data(json_data_folder+json_data_name,this);
				
			}

			FindObjectOfType<Jump_tag_list> ().refresh ();

		}

		public void clear_list(){

			if(list!=null){

				foreach(Action a in action_list){
					a.destroy_called_by_parent=true;
				}
				
				DestroyImmediate(list.gameObject);
				
			}

			GameObject temp = new GameObject ();
		
			list = temp.transform;
		
			list.parent = transform;

		}

		public void create_list_if_null(){
			
			if(list==null){
				
				GameObject temp = new GameObject ();
				
				list = temp.transform;
				
				list.parent = transform;
				
			}

		}

	

		public void refresh_children(Game_event input_parent_game_event){

			parent_game_event = input_parent_game_event;

			is_refresh_called_by_game_event = true;

			refresh_children ();
		}

		private void refresh_children(){

			//print (is_refresh_called_by_game_event);

			//print (is_refresh_called_by_self);


			if(is_refresh_called_by_game_event&&is_refresh_called_by_self){

				create_list_if_null();

				if(this!=null&&action_list!=null){

					action_list = Sakura.get_and_filter_monos_from_children<Action,Action> (list);

					list.name="list";

					int action_no = 0;

					foreach(Action action in action_list){
						
						action.name="action "+action_no+": "+action.GetType().Name;
						action.parent_action_list=this;
						action.action_list_order=action_no;
						action_no++;


					}
					is_refresh_called_by_game_event = false;
					is_refresh_called_by_self = false;

				}
			}


		}

		public T add_action<T>() where T:Action{

			create_list_if_null();

			GameObject temp = new GameObject ();
			temp.transform.parent = list;
			T output = (T) temp.AddComponent<T>();
			output.parent_action_list=this;
			action_list.Add (output);

			
			refresh_children (parent_game_event);
			
			
			return output;

		}

		public Action add_action(System.Type input_type){
			
			create_list_if_null();

			GameObject temp = new GameObject ();
			temp.transform.parent = list;
			Action output = (Action) temp.AddComponent(input_type);
			output.parent_action_list=this;
			action_list.Add (output);
			
			
			refresh_children (parent_game_event);
			
			
			return output;
			
		}

		public void run(int pointer_shift){

			current_action_pointer.current_value+=pointer_shift;

			if(action_list.Count>current_action_pointer.current_value){
				action_list[current_action_pointer.current_value].run(this);
			}

			print ("run");

		}

		public void run_at(int input_pointer){
			

			
			if(input_pointer>=0&&action_list.Count>input_pointer){

				current_action_pointer.current_value=input_pointer;
				action_list[current_action_pointer.current_value].run(this);
			}
			
			
		}

		public void run_next(){
			
			run (1);
		}

		public void run(){


			run (0);
		}



		public void initalize_pointer(){

			if(current_action_pointer==null&&this!=null){
					
				GameObject temp=new GameObject();
				
				Transform parent_transform=transform;
				
				if(parent_game_event!=null){
					
					if(parent_game_event.parent_game_event_list!=null){
						
						if(parent_game_event.parent_game_event_list.variable_list!=null){
							parent_transform=parent_game_event.parent_game_event_list.variable_list.transform;
						}
					}
				}
				
				
				
				temp.transform.parent=parent_transform;
				
				current_action_pointer=temp.AddComponent<Variable_int>();
				
				current_action_pointer.variable_name="current_action_pointer";
				current_action_pointer.name="current_action_pointer";

			}

			current_action_pointer.current_value = 0;
		}

	}
}
