﻿using UnityEngine;
using System.Collections;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.GAME_EVENT.CONDITION{
	public class Condition_item : Condition {

		// Use this for initialization


		public Variable_item variable_item;

		public Consts.Data.Game_event.Variable.Variable_item_state condition_variable_item_state;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public override bool check_condition(){
			
			return (condition_variable_item_state==variable_item.current_state);
		}
	}
}
