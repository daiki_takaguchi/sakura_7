﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA{
	public class Game_event : MonoBehaviour {

		public Game_event_list parent_game_event_list;

		//public Variable_list parent_variable_list;

		public List<Variable> referencing_variable_list= new List<Variable>();

		public Condition_list condition_list;

		public Action_list action_list;



		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			/*
			if(!Application.isPlaying){
				refresh_children();
			}*/
		}

		public void refresh_children(Game_event_list parent_game_event_list){
			condition_list= Sakura.get_mono_from_children<Condition_list>(transform);

			if(condition_list!=null){
				condition_list.name="condition_list";

				condition_list.refresh_children (this);
			}
			
			action_list= Sakura.get_mono_from_children<Action_list>(transform);

			if(action_list!=null){
				action_list.name = "action_list";

				action_list.refresh_children (this);
			}
		}
		
		public void initialize(){
			foreach (Transform child in transform){
				DestroyImmediate(child);
			}

			GameObject condition_list_gameobject = new GameObject ();
			condition_list_gameobject.transform.parent = transform;
			condition_list=condition_list_gameobject.AddComponent<Condition_list> ();
			condition_list.name="condition_list";
			condition_list.refresh_children (this);

			GameObject action_list_gameobject = new GameObject ();
			action_list_gameobject.transform.parent = transform;
			action_list=action_list_gameobject.AddComponent<Action_list> ();
			action_list.name = "action_list";
			action_list.refresh_children (this);

		}

		public Action_list get_available_action_list(){

			Action_list output = null;

			if(condition_list.check_conditions()){
				output=action_list;
			}

			return output;
		}

		public void add_event_to_variables(){
			List<Variable> variable_list=condition_list.add_event_to_variables (this);

			if(variable_list.Count>0){
				referencing_variable_list.AddRange(variable_list);
			}
		}

		public T add_condition<T>() where T:Condition{
			
			T output = null;

			if(condition_list!=null){
				output=condition_list.add_condition<T>();
				condition_list.refresh_children (this);
			}
			
			return output;
			
		}

		public T add_action<T>() where T:Action{
			
			T output = null;
			
			if(action_list!=null){
				output=(T) action_list.add_action<T>();
				action_list.refresh_children(this);
			}
			
			return output;
			
		}

		public void run_event(){

			Action_list available_action_list = get_available_action_list ();

			if(available_action_list!=null){
				available_action_list.run();
			}
		}

		public void variable_changed(Variable input_variable){
			
		}

		public void initalize_action_pointer(){
			action_list.initalize_pointer ();
		}

		void OnDestroy(){
			foreach(Variable variable in referencing_variable_list){
				variable.waiting_game_event_list.remove_game_event(this);
			}
		}
	}
}
