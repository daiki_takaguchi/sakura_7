﻿using UnityEngine;
using System.Collections;




namespace SAKURA{
	public class Data : MonoBehaviour {


		public static Data data_static;
		public Sakura sakura;
		public DATA.Game_settings game_settings;
		public DATA.Save_data save_data;
		public DATA.GAME_EVENT.ACTION.Jump_tag_list jump_tag_list;
		public bool is_initialized=false;


		// Use this for initialization
		void Start () {
			if(data_static==null){
				data_static=this;
			}

		}
		
		// Update is called once per frame
		void Update () {
			if(!is_initialized){
				save_data.initialize();
				is_initialized=true;
			}
		
		}



	}
}
