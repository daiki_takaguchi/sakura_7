﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.SAVE_DATA;


namespace SAKURA.DATA{
	[ExecuteInEditMode]

	public class Save_data : MonoBehaviour {

		public Save_data_slot system_data;

		public Transform slot_list_transform;

		public List<GameObject> slot;

		public Save_data_slot current_play_data;

		public Save_data_slot quick_play_data;

	
		public bool add_slot_now;



		// Use this for initialization
		void Start () {

		}
			

		// Update is called once per frame
		void Update () {

			if(!Application.isPlaying){
				if(add_slot_now){

					List<GameObject> temp_list = new List<GameObject>(slot);

					foreach (GameObject temp in temp_list){
						if(temp==null){
							slot.Remove(temp);
						}
					}


					add_slot();
					add_slot_now=false;
				}
			}

		
		}


		public bool is_chapter_no(int input_chapter_no){

			bool output = true;


			return output;
		}



		public void new_play_data(int input_list_no, int input_chapter_no){

			if(0<=input_list_no&&input_list_no<slot_list_transform.childCount){

			}

		}

		public void remove_play_data(int input_list_no){

			if(0<=input_list_no&&input_list_no<slot_list_transform.childCount){
				
			}
		}

		public void initialize(){
			system_data.load ();

		
		}

		public void add_slot(){
			GameObject temp = Instantiate (system_data.gameObject);

			temp.name = "slot_" +( slot_list_transform.childCount);


			temp.GetComponent<Save_data_slot>().save_data_prefix=temp.name+"_";

			temp.GetComponent<Save_data_slot> ().variable_list = null;

			temp.transform.parent = slot_list_transform;



			slot.Add (temp);

		}

	


	}
	
}

