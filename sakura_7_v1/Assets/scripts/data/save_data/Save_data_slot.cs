﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.SAVE_DATA{
	[ExecuteInEditMode]

	public class Save_data_slot: MonoBehaviour {


		// Use this for initialization

		public Variable_list variable_list;
	
		public string save_data_prefix;

		public bool save_now;
		public bool load_now;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if (!Application.isPlaying) {
				if (save_now) {
					save ();

					save_now = !save_now;
				}

				if (load_now) {
					load ();
					
					load_now = !load_now;
				}
			} else {

				if (save_now) {
					save ();
				
					save_now = !save_now;
				}
			
				if (load_now) {
					load ();
				
					load_now = !load_now;
				}
			}
		
		}

		public void save(){
			variable_list.save (save_data_prefix);
			PlayerPrefs.Save ();
		}

		public void load(){
			variable_list.load (save_data_prefix);
		}

		public Variable get_variable(string input_variable_name){

			return variable_list.get_variable(input_variable_name);

		}




	}

}
