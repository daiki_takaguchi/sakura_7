﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.SCENE_SETTINGS{
	[ExecuteInEditMode]
	public class Scene_settings : Adv_settings_base {

		// Use this for initialization



		public Variable_int scene_no;


		public List<Scene_sequence> scene_sequence_list;

		public List<Scene_settings> prev_scene_settings;
		public List<Scene_settings> next_scene_settings;

		void Start () {
		
		}
		
		// Update is called once per frame
		/*
		void Update () {
		
			if(!Application.isPlaying){



				if(add_action_now){

					add_action_now=false;


					if(action_type_to_add==Consts.Data.Scene_settings.Action.Action_type.change_person_image){
						GameObject temp= new GameObject();
						temp.transform.parent=transform;
						temp.AddComponent<SCENE_SETTINGS.Change_person_image>();

					}else if(action_type_to_add==Consts.Data.Scene_settings.Action.Action_type.wait){
						GameObject temp= new GameObject();
						temp.transform.parent=transform;
						temp.AddComponent<SCENE_SETTINGS.Wait>();
					}else if(action_type_to_add==Consts.Data.Scene_settings.Action.Action_type.adv_text_action){
						GameObject temp= new GameObject();
						temp.transform.parent=transform;
						temp.AddComponent<SCENE_SETTINGS.Adv_text_action>();
					}
				
				}

				/*
				if(prev_actions_count==actions.Count){




					foreach (SCENE_SETTINGS.Action action in actions){
						
						if(action!=null){
							action.name="";
							//count++;
							
							//new_actions.Add (action);
						}
					}

					int count=0;

					List<SCENE_SETTINGS.Action> new_actions=new List<SAKURA.DATA.SCENE_SETTINGS.Action>();

		
					foreach (SCENE_SETTINGS.Action action in actions){

						if(action!=null){
							action.name+="Action:"+count+" "+ action.action_type.ToString()+" ";
							count++;

							new_actions.Add (action);
						}
					}

					actions=new_actions;
						
						
				}else{

					//print ("!");
					List<SCENE_SETTINGS.Action> new_actions=new List<SAKURA.DATA.SCENE_SETTINGS.Action>();

					int count=0;
					foreach (SCENE_SETTINGS.Action action in actions){


						new_actions.Add (action);

						count++;

					}

					actions=new_actions;
					prev_actions_count=actions.Count;

				}


			}

				List<SCENE_SETTINGS.Action> new_actions=new List<SAKURA.DATA.SCENE_SETTINGS.Action>();
				int count=0;

				foreach(Transform child in transform){

					SCENE_SETTINGS.Action action=child.GetComponent<SCENE_SETTINGS.Action>();

					if(action!=null){
						action.name="Action:"+count+" "+ action.action_type.ToString()+" ";
						count++;
						
						new_actions.Add (action);
					}

				}
				
				actions=new_actions;
			}

			
		}
		*/

		public override void refresh(Adv_settings_base input_parent){
			variable_list = input_parent.variable_list;

			if(scene_no==null){
				GameObject temp=new GameObject();
				temp.transform.parent=variable_list.transform;
				
				scene_no=temp.AddComponent<Variable_int>();
				
				scene_no.variable_name="scene_no";
				
				
			}

			int sequence_no=0;
			
			scene_sequence_list=Sakura.get_monos_from_children<Scene_sequence>(transform);
			
			foreach (Scene_sequence sequence in scene_sequence_list){
				
				sequence.refresh (this);
							
				sequence.sequence_no.current_value=sequence_no;
				
				sequence.gameObject.name="sequence "+sequence_no;
				

				
				sequence_no++;
				
				
				
				
				
				
			}
		}

		public void run(){
			foreach(Scene_sequence scene_sequence in scene_sequence_list){
				scene_sequence.run ();
			}
		}
		


	}
}
