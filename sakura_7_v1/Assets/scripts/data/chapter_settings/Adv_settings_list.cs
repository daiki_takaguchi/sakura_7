﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;

namespace SAKURA.DATA.SCENE_SETTINGS{

	[ExecuteInEditMode]
	public class Adv_settings_list : Adv_settings_base {

		public List<Chapter_settings> chapter_settings_list;


		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

			if(!Application.isPlaying){

				chapter_settings_list= Sakura.get_monos_from_children<Chapter_settings>(transform);

				foreach(Chapter_settings chapter in chapter_settings_list){

					chapter.refresh(this);

				}
			}
			
		
			
		}
		


	}
}
