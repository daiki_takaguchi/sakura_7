﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.SCENE_SETTINGS{
	[ExecuteInEditMode]
	public class Scene_sequence :Adv_settings_base {


		///public int parallel_no;
		public Variable_int sequence_no;

		public Scene_settings scene_settings;

		public List<Scene_sequence_state> state_list;

		//public int current_state_pointer=-1;

		public Variable_int current_state_pointer;
		public Scene_sequence_state current_state;


		[HideInInspector] public string current_state_name;


		// Use this for initialization
		void Start () {
		
		}


		
		// Update is called once per frame
		void Update () {
			
			if (!Application.isPlaying) {
				/*
				if(variables_current_scope==null){
					GameObject temp = new GameObject();
					temp.transform.parent=transform;
					variables_current_scope=temp.AddComponent<Variable_list>();
					
				}*/

				if(current_state==null){
					if(state_list.Count>0){
						current_state=state_list[0];
						current_state_name=state_list[0].name;
					}
				}

			}
		}

		public override void refresh(Adv_settings_base input_parent){
			variable_list = input_parent.variable_list;

			if(sequence_no==null){
				GameObject temp=new GameObject();
				temp.transform.parent=variable_list.transform;
				
				sequence_no=temp.AddComponent<Variable_int>();
				
				sequence_no.variable_name="sequence_no";
				
				
			}

			if(current_state_pointer==null){
				GameObject temp=new GameObject();
				temp.transform.parent=variable_list.transform;
				
				current_state_pointer=temp.AddComponent<Variable_int>();
				
				current_state_pointer.variable_name="current_state_pointer";
				current_state_pointer.name="current_state_pointer";

				current_state_pointer.current_value=-1;



				GameObject temp_2=new GameObject();
				
				temp_2.transform.parent=transform;
				
				Change_variable_int temp_2_action=temp_2.AddComponent<Change_variable_int>();

				action=temp_2_action;
				
				action.name= "state_pointer_increment";

				temp_2_action.target_variable= current_state_pointer;
				
			}

			int state_no=0;
			
			state_list=Sakura.get_monos_from_children<Scene_sequence_state>(transform);

			foreach (Scene_sequence_state sequence_state in state_list){
				
				sequence_state.refresh(this);

				sequence_state.state_no.current_value=state_no;
				
				sequence_state.gameObject.name="state "+state_no;
				
				
				state_no++;
				
				
				
			}

		}

		public void run(){
			current_state.run ();
		}


		public void run_transition(){
			current_state_pointer.current_value++;

			if(current_state_pointer.current_value<state_list.Count){
				current_state=state_list[current_state_pointer.current_value];
				current_state_name=state_list[current_state_pointer.current_value].name;
			}else{

			}
		}

	}
}
