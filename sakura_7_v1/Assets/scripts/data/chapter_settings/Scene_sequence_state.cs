﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.DATA.GAME_EVENT.CONDITION;


namespace SAKURA.DATA.SCENE_SETTINGS{
	[ExecuteInEditMode]
	public class Scene_sequence_state : Adv_settings_base {

		public Variable_int state_no;

		public Scene_sequence scene_sequence;

		public Game_event_list game_event_list;
	
		// Use this for initialization
		void Start () {
		
		}

		public void check_transition(){

		}

		public void run(){
			game_event_list.run_events ();
		}

		public void run_transition(){

		}

		public void check_events(){

		}

		public override void refresh(Adv_settings_base input_parent){



			variable_list = input_parent.variable_list;

			
			scene_sequence = (Scene_sequence) input_parent;

			if(state_no==null){
				GameObject temp=new GameObject();
				temp.transform.parent=variable_list.transform;

				state_no=temp.AddComponent<Variable_int>();

				state_no.variable_name="state_no";


			}

			game_event_list.variable_list = variable_list;
			

		}

		
		// Update is called once per frame

	}
}
