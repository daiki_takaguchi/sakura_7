﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.DATA.GAME_EVENT.ACTION;



namespace SAKURA.DATA.SCENE_SETTINGS{
	[ExecuteInEditMode]
	public class Adv_settings_base : MonoBehaviour {

		// Use this for initialization

		//public Consts.Data.Game_event.Variable.Variable_scope_type variable_scope_type;
		public Variable_list  variable_list;
		public Action action;
		public List<Variable> variables_referencing;


		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {



		}

		public virtual void refresh(Adv_settings_base input_base){

		}
	}
}
