﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.DATA.GAME_EVENT.ACTION;

namespace SAKURA.DATA.SCENE_SETTINGS{
	[ExecuteInEditMode]
	public class Chapter_settings : Adv_settings_base {

		// Use this for initialization

		public Variable_int chapter_no;

		public List<Scene_settings> scene_setting_list;


		void Start () {
		
		}

		public override void refresh(Adv_settings_base input_parent){
			//variable_list = input_parent.variable_list;
			if(variable_list!=null){
				variable_list.refresh_children ();
			

				if(chapter_no==null){
					GameObject temp=new GameObject();

					temp.transform.parent=variable_list.transform;
					
					chapter_no=temp.AddComponent<Variable_int>();
					
					chapter_no.variable_name="chapter_no";




				}

				int scene_no=0;
				
				scene_setting_list=Sakura.get_monos_from_children<Scene_settings>(transform);
				
				
				foreach (Scene_settings scene in scene_setting_list){
					
					scene.refresh(this);
					
					scene.scene_no.current_value=scene_no;
					
					scene.gameObject.name="scene "+scene_no;
					
					//scene.variables_current_scope=chapter.variables_current_scope;
					//scene.variables_current_scope.variable_scope_type=scene.variable_scope_type;
					//scene.variables_current_scope.gameObject.name="variable_list "+scene.gameObject.name;

					
					scene_no++;
					

					
				}
			}
		}


		
		// Update is called once per frame
		/*
		void Update () {
						
			if(!Application.isPlaying){
				gameObject.name="chapter "+chapter_no;

				int scene_no=0;

				foreach(Scene_settings_chart chart in scene_settings_chart){

					scene_no++;
				
					chart.chaptor_no=chapter_no;
					chart.scene_no=scene_no;
					chart.gameObject.name="scene "+chart.scene_no;
					chart.chapter_settings=this;

					int parallel_no=0;

					foreach (Scene_settings settings in chart.scene_settings_parallel){

						parallel_no++;

						settings.chaptor_no=chapter_no;
						settings.scene_no=scene_no;
						settings.parallel_no=parallel_no;

						settings.scene_settings_chart=chart;

						settings.gameObject.name="parallel "+parallel_no;


						int sequence_no=0;

						foreach (Scene_sequence sequence in settings.scene_sequences){
							
							sequence_no++;
							
							sequence.chaptor_no=chapter_no;
							sequence.scene_no=scene_no;
							sequence.parallel_no=parallel_no;
							sequence.sequence_no=sequence_no;

							sequence.scene_settings=settings;
							
							sequence.gameObject.name="sequence "+sequence_no;

							int state_no=0;
							
							foreach (Scene_sequence_state sequence_state in sequence.states){
								
								state_no++;
								
								sequence_state.chaptor_no=chapter_no;
								sequence_state.scene_no=scene_no;
								sequence_state.parallel_no=parallel_no;
								sequence_state.sequence_no=sequence_no;
								sequence_state.state_no=state_no;

								sequence_state.scene_sequence=sequence;
								
								sequence_state.gameObject.name="state "+state_no;
							}

						
						}
					}
				}
			}
			

		

		}
		*/
	}
}
