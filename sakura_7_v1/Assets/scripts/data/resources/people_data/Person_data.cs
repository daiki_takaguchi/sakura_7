﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.RESOURCES{
	public class Person_data : MonoBehaviour {

		public string  person_name;

		//public Variable_list image_list;

		public Texture normal;
		public Texture akire;
	
		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public Texture get_person_image(SAKURA.Consts.Data.Game_event.Action.Person_image_expression input_expression){
			Texture output = null;

			if(input_expression==Consts.Data.Game_event.Action.Person_image_expression.normal){
				output=normal;
			}else if(input_expression==Consts.Data.Game_event.Action.Person_image_expression.akire){
				output=akire;
			}


			return output;

		}

		public string get_person_name(){
			return person_name;
		}
	}
}