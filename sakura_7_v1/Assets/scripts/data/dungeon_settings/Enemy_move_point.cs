﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



namespace SAKURA.DATA.DUNGEON_SETTINGS{
	[ExecuteInEditMode]
	public class Enemy_move_point : MonoBehaviour {


	
		public bool refresh_points_now=false;

		public int id = -1;

		public List<Enemy_move_point> connected_points;

		//public List<Collider> colliders;


		// id distance, length
		public Vector3[] distance;

		public Enemy_settings enemy_settings;

		[HideInInspector] public List<Enemy_move_line> connected_lines;

		//[HideInInspector] public bool refresh_point_now=false;

		[HideInInspector] public GameObject line_gameobject;
		//public GameObject distance_gameobject;

		[HideInInspector] public List<Enemy_move_point> all_points;

	
		public GameObject point_object;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

			/*
			if(refresh_point_now){
				refresh_point();
				refresh_point_now=false;
			}*/

			if(refresh_points_now){
				enemy_settings.dungeon_settings.refresh_points();
				refresh_points_now=false;
			}

			/*
			if(add_box_now){
				//GameObject temp= new GameObject();
				//temp.transform.parent=this.transform;
				//temp.transform.localPosition= new Vector3(0,0,0);
				BoxCollider box=gameObject.AddComponent<BoxCollider>();
				box.isTrigger=true;
				box.center = new Vector3(0,0,0);
				colliders.Add (box);

				add_box_now=false;

			}*/
		
		}

		public void objects_active(bool input){
			point_object.SetActive (input);	

			foreach(Enemy_move_line line in connected_lines){
				line.child.gameObject.SetActive (input);
			}
		}

		public void refresh_point(){


			List<Enemy_move_point> temp = new List<Enemy_move_point> ();

			foreach(Enemy_move_point point in connected_points){
				if(point!=null){
					temp.Add (point);
				}
			}

			connected_points=temp;

			/*
			List<Collider> temp_cols = new List<Collider> ();

			foreach(Collider col in colliders){
				if(col!=null){
					temp_cols.Add (col);
				}
			}

			colliders = temp_cols;
			*/
			refresh_lines ();
		}

		public void refresh_lines(){

			foreach (Enemy_move_line line in connected_lines) {
				if(line!=null){
					DestroyImmediate(line.gameObject);
				}
			}

			connected_lines = new List<Enemy_move_line> ();

			foreach(Enemy_move_point point in connected_points){

				GameObject temp=(GameObject) Instantiate(line_gameobject);
				temp.name="line_"+this.id+"_to_"+point.id;
				temp.transform.parent=transform;
				temp.transform.localPosition= new Vector3(0,0,0);

				if(temp.GetComponent<Enemy_move_line>()==null){
					temp.AddComponent<Enemy_move_line>();
				}

				temp.GetComponent<Enemy_move_line>().set_points(this,point);


				connected_lines.Add (temp.GetComponent<Enemy_move_line>());
			
			}
		}

		public void set_id(int input_id){
			id = input_id;
		}



		public void calculate_distance(List<Enemy_move_point> input_points){

			all_points = input_points;

			if(id!=-1){

				bool[] is_point_calculated = new bool[input_points.Count];

				//List<Enemy_move_point> remaining_points = input_points;

				List<Enemy_move_point> current_points = new List<Enemy_move_point> ();
				distance= new Vector3[input_points.Count];

				//print (input_points.Count);
				//print (distance[2].enabled);

				int max_loop = 100;
				int caluculated_points_count = 0;

				foreach(Enemy_move_point point in input_points){

					if(point==this){
						current_points.Add(point);
						distance[this.id]= new Vector3(this.id,0,0);
						is_point_calculated[this.id]=true;
						caluculated_points_count++;
					}

				}

				if(current_points.Count>0){

					for (int i=0; i<max_loop;i++){


						List<Enemy_move_point> next_caluculate_points = new List<Enemy_move_point> ();

						foreach(Enemy_move_point point in current_points){

							foreach(Enemy_move_point point_conneted_to_current in point.connected_points){

								if(point_conneted_to_current.id>=0&&point_conneted_to_current.id<=input_points.Count){

									if(!is_point_calculated[point_conneted_to_current.id]){

										next_caluculate_points.Add (point_conneted_to_current);

										distance[point_conneted_to_current.id]= new Vector3(point_conneted_to_current.id,i+1,(point_conneted_to_current.transform.position-this.transform.position).magnitude);

										is_point_calculated[point_conneted_to_current.id]=true;

										caluculated_points_count++;
									}

								}else{
									Debug.LogError ("id does not match the number of points. id: "+point_conneted_to_current.id+ " array size: " +input_points.Count );
								}
							}



						}

						current_points=next_caluculate_points;

						if(caluculated_points_count>=input_points.Count){
							break;
						}

						if(current_points.Count==0){

							Debug.LogError ("some points are not connected!");
					
							break;
						}


					}

				}else{
					Debug.LogError ("input points do not contain me!");
				}


				for(int i=0; i<is_point_calculated.Length ;i++){

					if(!is_point_calculated[i]){

						distance[i]= new Vector3(i,-2,(input_points[i].transform.position-this.transform.position).magnitude);

						Debug.LogError("point id "+this.id+ " is not connected to point id "+ i);
					}
				}


			}


		}

		public Enemy_move_point get_closest_point_from_connected(Enemy_move_point input_point){

			Enemy_move_point output = connected_points[0];

			float min_distance=-1;

			foreach (Enemy_move_point point in connected_points){

				float temp=point.distance[input_point.id].y;

			
				if(min_distance==-1||min_distance>temp){
					output=point;
					min_distance=temp;
				}

			}

			
		
			return output;
		}


	}


	/*
	public class Distance_between_points:MonoBehaviour{

		private int distance;   // the name field
		public int Distance   // the Name property
		{
			get 
			{
				return distance; 
			}
		}

		private int from;   // the name field
		public int From   // the Name property
		{
			get 
			{
				return from; 
			}
		}

		private int to; 
		public int To   // the Name property
		{
			get 
			{
				return to; 
			}
		}

		public Distance_between_points(int input_from, int input_to, int input_distance){
			from = input_from;
			to = input_to;
			distance = input_distance;
		}


			
	}
	*/
}
