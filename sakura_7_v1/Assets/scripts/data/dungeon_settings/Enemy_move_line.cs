﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SAKURA.DATA.DUNGEON_SETTINGS{
	public class Enemy_move_line : MonoBehaviour {


		public Enemy_move_point point_from;
		public Enemy_move_point point_to;
	
		public Transform child;


		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void set_points(Enemy_move_point input_from, Enemy_move_point input_to){

			point_from = input_from;
			point_to = input_to;

			Vector3 from_position = input_from.transform.position;
			Vector3 to_position = input_to.transform.position;

			float line_width = 0.5f;
			float line_length= (to_position-from_position).magnitude/2.0f;  


			//child.localPosition = (from_position *(3.0f/4.0f)+ to_position/ 4.0f) - from_position;

			child.localPosition = new Vector3 (0,0,line_length/2.0f);
		



			child.localScale = new Vector3 ( line_width, line_width,line_length);

			transform.localRotation = Sakura.from_to_rotation_y(new Vector3(0,0,1),to_position-from_position);

		}

	}
}
