﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.UI.OBJECTS;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.DATA.GAME_EVENT.ACTION;

namespace SAKURA.UI.LAYER{
	public class Adv_text_layer : Layer {

		// Use this for initialization

		public Text_window person_name;
		public Text_window main_text;
		public GameObject select_gameobject;
		public List<Text_window> select= new List<Text_window>();
	
		public List<Button> select_button = new List<Button> ();


		public Variable_backlog variable_backlog;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void set_name(SAKURA.Consts.Data.Game_event.Action.Person_name input_person_name){

			string temp="";

			person_name.set_text(SAKURA.DATA.Sakura_resources.resources_static.get_person_name (input_person_name)
			                     ,Consts.Ui.Objects.Text_mode.all_at_once,out temp);

		
		}

		public void set_name(string input_name){
			
			string temp="";
			
			person_name.set_text(input_name ,Consts.Ui.Objects.Text_mode.all_at_once,out temp);
			
			
		}

		public void show_select(List<string> input_text_list, List<string> input_tag_list){

			select_gameobject.SetActive (true);

			for (int i =0;i<3 ;i++){

				string out_text="";

				select[i].set_text(input_text_list[i],Consts.Ui.Objects.Text_mode.all_at_once,out out_text);

				Jump temp=(Jump) select_button[i].game_event_list.game_event_list[0].action_list.action_list[0];

				temp.jump_type=Consts.Data.Game_event.Action.Jump_type.tag;

				temp.action_tag=input_tag_list[i];


			}

		}

		public void hide_select(){
			select_gameobject.SetActive (false);
		}

		public override List<SAKURA.Consts.Ui.Layer_type> show(){

			gameObject.SetActive (true);

			return layers_conflicting;
		}

		public void clear_text(){
			person_name.clear_text ();
			main_text.clear_text ();
		}





	
	}
}
