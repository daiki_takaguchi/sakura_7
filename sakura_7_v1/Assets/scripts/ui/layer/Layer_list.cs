﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.UI;

namespace SAKURA.UI.LAYER{
	public class Layer_list : MonoBehaviour {

		// Use this for initialization

		//public bool hide_all_now = false;
		//public bool show_all_now=false;
		
		//public bool show_layer_now = false;


		public SAKURA.Ui ui;


		public Layer title_layer;
		public Adv_layer adv_layer;
		public Adv_text_layer adv_text_layer;
		public Dungeon_layer dungeon_layer;
		public Layer adv_menu_layer;
		public Touch_panel_layer touch_panel_layer;
		public Layer enemy_move_points_panels_layer;
		public Backlog_layer backlog_layer;
		
		public List<Layer> layers_shown;
		public List<Layer> layers_fading_in;
		public List<Layer> layers_fading_out;


		void Update () {
			if(layers_fading_in.Count>0){
				
			}
			
			if(layers_fading_out.Count>0){
				
			}
			

			
		}


		public Layer get_layer(SAKURA.Consts.Ui.Layer_type input_layer_type){
			Layer output=null;
			
			
			
			if(input_layer_type==Consts.Ui.Layer_type.title){
				return title_layer;
			}else if(input_layer_type==Consts.Ui.Layer_type.adv){
				return adv_layer;
			}else if(input_layer_type==Consts.Ui.Layer_type.adv_text){
				return adv_text_layer;
			}else if(input_layer_type==Consts.Ui.Layer_type.dungeon){
				return dungeon_layer;
			}else if(input_layer_type==Consts.Ui.Layer_type.backlog){
				return backlog_layer;
			}else if(input_layer_type==Consts.Ui.Layer_type.enemy_move_points_panels){
				return enemy_move_points_panels_layer;
			}
			
			
			return output;
		}


		public void show(SAKURA.Consts.Ui.Layer_type input_layer_type){
			show (get_layer (input_layer_type));
		}
		
		public void show(Layer input_layer){
			
			List<SAKURA.Consts.Ui.Layer_type> layers_conflicting = input_layer.show();
			
			foreach (SAKURA.Consts.Ui.Layer_type layer in layers_conflicting){
				hide(layer);
			}
			
		}
		
		
		public void hide(SAKURA.Consts.Ui.Layer_type input_layer_type){
			hide(get_layer (input_layer_type));
		}
		
		public void hide(Layer input_layer){
			
			input_layer.hide();
			
			
		}
		
		public void hide_all(){
			title_layer.hide ();
			adv_layer.hide ();
			adv_text_layer.hide ();
			enemy_move_points_panels_layer.hide ();
		}
		


	
	}
	
}
