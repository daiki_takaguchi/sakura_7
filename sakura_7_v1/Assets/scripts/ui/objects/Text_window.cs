﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SAKURA.UI.OBJECTS{
	public class Text_window : MonoBehaviour {

		//public Text_line[] text_lines;

		public UIFont font;
		
		public UIWidget.Pivot pivot;
		
		public int default_font_size;
		//public int current_font_size;
		public Color default_color;



		public float max_height;
		public float max_width;
		public float max_char;
		public bool is_max_char_mode;
		public float space_between_lines;
		public SAKURA.Consts.Ui.Layer_depth_type depth;

		public bool japanese_hyphenation=true;
		public bool parentheses_indentation=true;


		public string input_text="";
		public string current_text="";



		//public int max_lines;


		private float current_height_without_current_line;
		public float current_height;
		//public float current_width;



		public float current_line_height=0.0f;

	
		public List<Text_line> text_lines = new List<Text_line> ();
		public Text_line current_line=null;
	
		public int total_line_count;
		 


		private int system_max_loop=1000;

		public List<Text_tag> text_tags= new List<Text_tag>();
		public bool is_parentheses_mode=false;

		//private is_initialized=false;


		//public GameObject text_line_gameobject;

		// Use this for initialization
		void Start () {

			//string remaining_text = "";

			//Add_text ("text",true,out remaining_text);

			//clear_text ();

		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void clear_text(){

			foreach(Text_line line in text_lines){

				if(line!=null){
					DestroyImmediate(line.gameObject);
				}
			}

			text_lines= new List<Text_line>();

			/*
			if(gameObject.name=="person_name"){
				print ("cleared");
			}*/

			current_text = "";
			current_height_without_current_line = 0;
			current_height = 0;
			//current_width = 0.0f;
			current_line_height = 0.0f;
			current_line = null;
			text_tags = new List<Text_tag> ();
			text_tags.Add (new Text_tag (default_font_size,default_color,Consts.Ui.Objects.Text_vertical_align.none));

		}
	

		public bool set_text(string input_text, Consts.Ui.Objects.Text_mode input_mode, out string remaining_text){

			clear_text ();

			return add_text (input_text, input_mode, out  remaining_text);

		}


		public bool add_text(string temp_input_text,Consts.Ui.Objects.Text_mode  input_mode, out string remaining_text){

			//print ("set_text_called");
			input_text = temp_input_text;
			string current_remaining_text = input_text;
			string added_text = "";

			bool is_text_finished = false;

			int loop_count = 0;



			//current_font_size = default_font_size;

			bool finish_now = false;



			if(current_line==null){

				current_line=create_text_line ();
				
				Vector3 temp =current_line.transform.localPosition;
				
				current_line.transform.localPosition= new Vector3(temp.x,current_line_height,temp.z);

				current_height_without_current_line=0.0f;
				text_lines.Add (current_line);
			
				input_text = temp_input_text;
			}


			//print (current_height_without_current_line + current_line.height);

			while((!finish_now)&&(!is_text_finished)&&(current_height_without_current_line+current_line.height)<=max_height){

				if(loop_count>system_max_loop){
					print ("max_loop!"+current_remaining_text);
					finish_now=true;
				}

				//print (current_remaining_text);
				if(current_line.Add_text(current_remaining_text,input_mode,text_tags,is_parentheses_mode,out added_text, out current_remaining_text, out text_tags,out is_parentheses_mode)){


					if(current_line.is_full){



						if(current_height_without_current_line+current_line.height+space_between_lines+text_tags[text_tags.Count-1].font_size<=max_height){
							
							current_line_height-=current_line.height/2.0f+space_between_lines+text_tags[text_tags.Count-1].font_size/2.0f;
						
							current_height_without_current_line+=current_line.height+space_between_lines;

							current_line=create_text_line ();
							
							Vector3 temp =current_line.transform.localPosition;
							
							current_line.transform.localPosition= new Vector3(temp.x,current_line_height,temp.z);
							
							text_lines.Add (current_line);



							if(current_line.Add_text(current_remaining_text,input_mode,text_tags,is_parentheses_mode,out added_text, out current_remaining_text, out text_tags,out is_parentheses_mode)){

							};
							
						}else{


							is_text_finished=true;
						}
						
					}else{

					
					}






				}else{

				};


				if(input_mode==Consts.Ui.Objects.Text_mode.type_writer){
					finish_now=true;
				}

				current_text+=added_text;

				if(current_remaining_text.Length==0){
					is_text_finished=true;
				}


				


			
				loop_count++;



			}


			current_height = current_height_without_current_line + current_line.height;

			remaining_text = current_remaining_text;


			return is_text_finished;

		}

		public Text_line create_text_line(){
			GameObject temp= new GameObject();

			temp.transform.parent = this.transform;
			temp.name = "text_line_" + text_lines.Count;
			temp.layer = gameObject.layer;
			temp.transform.localPosition = new Vector3 (0,0,0);
			temp.transform.localScale = new Vector3 (1,1,1);

			Text_line temp_line = temp.AddComponent<Text_line> ();
			temp_line.parent_text_window=this;
			temp_line.pivot = pivot;
			temp_line.font = font;
			temp_line.depth = depth;
			temp_line.japanese_hyphenation = japanese_hyphenation;
			temp_line.parentheses_indentation = parentheses_indentation;
			temp_line.max_width = max_width;

			return  temp_line;
		}




	}
}
