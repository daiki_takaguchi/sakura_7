﻿using UnityEngine;
using System.Collections;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.GAMEPLAY.DUNGEON;
using SAKURA.UI.LAYER;

namespace SAKURA.UI.OBJECTS{
	public class Touch_panel : MonoBehaviour {

		// Use this for initialization


		//public UILabel label;
		//public UIPanel panel;

		public Touch_panel_layer layer;

		public Consts.Ui.Objects.Touch_panel_state prev_state;
		public Consts.Ui.Objects.Touch_panel_state state;

		public Collider panel_collider;

		public Variable_touch_panel variable_touch_panel;

		public Camera current_camera;

		public bool is_debug_mode;

		public Finger first_finger=new Finger();

		public Finger second_finger=new Finger();


		void Start () {
			//print (label.relativeSize);



			if(is_debug_mode){
				layer.debug_text.gameObject.SetActive (true);
			}

		}
		
		// Update is called once per frame
		void Update () {


			check_and_change_touch_panel_state ();
		
		}



		public void check_finger_state(Finger input_first_finger,  Finger input_second_finger,out Finger output_first_finger, out Finger output_second_finger){

			output_first_finger = input_first_finger;
			output_second_finger = input_second_finger;



			if(Input.GetMouseButtonDown(0)){

				output_first_finger.state=Consts.Ui.Objects.finger_state.press_started;

				output_first_finger.press_start_position=Input.mousePosition;

				output_first_finger.press_start_time=Time.time;

				if(Gameplay.gameplay_static.current_game_state==Consts.Gameplay.Game_state_type.dungeon_test ||
				   Gameplay.gameplay_static.current_game_state==Consts.Gameplay.Game_state_type.dungeon){

					RaycastHit hit= new RaycastHit();
					Ray ray=  current_camera.ScreenPointToRay(Input.mousePosition);

					if(Physics.Raycast(ray, out hit)){

						item_object temp= hit.transform.gameObject.GetComponent<item_object>();

						if(temp!=null){
							temp.clicked();
						}
						
					}
					
				}
			
			
			
			}else if(Input.GetMouseButton(0)){

				output_first_finger.state=Consts.Ui.Objects.finger_state.pressing;

			}else if(Input.GetMouseButtonUp(0)){

				output_first_finger.state=Consts.Ui.Objects.finger_state.unpressed;

			}else{

				output_first_finger.state=Consts.Ui.Objects.finger_state.none;
			}

			if(Input.GetMouseButtonDown(1)){
				output_second_finger.state=Consts.Ui.Objects.finger_state.press_started;

				output_second_finger.press_start_position=Input.mousePosition;
				
				output_second_finger.press_start_time=Time.time;

			}else if(Input.GetMouseButton(1)){

				output_second_finger.state=Consts.Ui.Objects.finger_state.pressing;

			}else if(Input.GetMouseButtonUp(1)){
				output_second_finger.state=Consts.Ui.Objects.finger_state.unpressed;
			}else{
				output_second_finger.state=Consts.Ui.Objects.finger_state.none;
			}
		}

		public void check_and_change_touch_panel_state(){
			
			state = prev_state;
			bool temp_flag = false;
			
			if(is_debug_mode){
				string temp="";
				layer.debug_text.set_text (prev_state.ToString (),Consts.Ui.Objects.Text_mode.all_at_once,out temp);
			}


			
			check_finger_state(first_finger, second_finger,out first_finger,out second_finger);


			if(first_finger.state==Consts.Ui.Objects.finger_state.press_started){

				state=Consts.Ui.Objects.Touch_panel_state.one_finger_press_started;

			}else if(first_finger.state==Consts.Ui.Objects.finger_state.pressing){
				
				if(Time.time>first_finger.press_start_time+Data.data_static.game_settings.long_press_detection_seconds){
					state=Consts.Ui.Objects.Touch_panel_state.one_finger_long_press;
				}

				//print (Time.time-first_finger.press_start_time);
				//print (first_finger.press_start_position);
				
				//if(Time.time<first_finger.press_start_time+Data.data_static.game_settings.swipe_detection_seconds){

					
					if(first_finger.current_position.x>first_finger.press_start_position.x+Data.data_static.game_settings.swipe_detection_pixel){
						state=Consts.Ui.Objects.Touch_panel_state.swipe_right;
						//first_finger.press_start_position=Input.mousePosition;
					}
					
					if(first_finger.current_position.x<first_finger.press_start_position.x-Data.data_static.game_settings.swipe_detection_pixel){
						state=Consts.Ui.Objects.Touch_panel_state.swipe_left;
						//first_finger.press_start_position=Input.mousePosition;
					}
					
					if(first_finger.current_position.y>first_finger.press_start_position.y+Data.data_static.game_settings.swipe_detection_pixel){
						state=Consts.Ui.Objects.Touch_panel_state.swipe_up;
						//first_finger.press_start_position=Input.mousePosition;
					}
					
					if(first_finger.current_position.y<first_finger.press_start_position.y-Data.data_static.game_settings.swipe_detection_pixel){
						state=Consts.Ui.Objects.Touch_panel_state.swipe_down;
						//first_finger.press_start_position=Input.mousePosition;
					}

					
				//}


			}else if (first_finger.state==Consts.Ui.Objects.finger_state.unpressed) {

			
				
				if(state==SAKURA.Consts.Ui.Objects.Touch_panel_state.one_finger_press_started){

					if(Time.time<first_finger.press_start_time+Data.data_static.game_settings.tap_detection_seconds){
						state=Consts.Ui.Objects.Touch_panel_state.one_finger_tap;
					}else{
						state=Consts.Ui.Objects.Touch_panel_state.none;
					}
					
				}else {
					//print(state+ " unpressed");
					state=Consts.Ui.Objects.Touch_panel_state.none;

					temp_flag=true;

				}
				
				
				
			}
			
			if(second_finger.state==Consts.Ui.Objects.finger_state.press_started){

				state=Consts.Ui.Objects.Touch_panel_state.two_finger_press_started;
			}else if(second_finger.state==Consts.Ui.Objects.finger_state.pressing){

				if(Time.time>second_finger.press_start_time+Data.data_static.game_settings.long_press_detection_seconds){
					state=Consts.Ui.Objects.Touch_panel_state.two_finger_long_press;
				}

			}else if (second_finger.state==Consts.Ui.Objects.finger_state.unpressed) {
				if(Time.time<second_finger.press_start_time+Data.data_static.game_settings.tap_detection_seconds){
					state=Consts.Ui.Objects.Touch_panel_state.two_finger_tap;
				
				}
				
				
				
			}
			if(temp_flag){
				print(state+" touch panel:"+variable_touch_panel.state+ " unpressed check");
			}
			
			variable_touch_panel.change_value (state);
			
			//if(state!=Consts.Ui.Objects.Touch_panel_state.none){
			prev_state=state;
			//}

		}
	





	}


	public class Finger {
		public Consts.Ui.Objects.finger_state state;

		public Vector2 current_position
		{
			get
			{
				return Input.mousePosition;
			}
		}


		public Vector2 press_start_position;

		public float press_start_time;

	}






}