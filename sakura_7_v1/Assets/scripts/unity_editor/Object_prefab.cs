﻿using UnityEngine;
using System.Collections;
using SAKURA.DATA;

namespace SAKURA.UNITY_EDITOR{
[ExecuteInEditMode]
	public class Object_prefab: MonoBehaviour {

		public SAKURA.Consts.Unity_editor.Object_prefab.Prefab_type prefab_type;



		[HideInInspector]public GameObject prefab;
		[HideInInspector] public string prefab_guid;
		[HideInInspector]public string prefab_name;

		public Vector3 local_position;
		public Vector3 local_rotation;
		public Vector3 local_scale= new Vector3(1,1,1);

		public Vector3 grid_length=new Vector3(1.28f,1.28f,1.28f);
	
		void Update () {
			if(!Application.isPlaying){

				refresh();
			
			}
		}

		public void refresh(){
			if(prefab_type==Consts.Unity_editor.Object_prefab.Prefab_type.map){
				grid_length=GameObject.FindObjectOfType<Game_settings>().grid_length;
			}else if(prefab_type==Consts.Unity_editor.Object_prefab.Prefab_type.object_in_map){
				grid_length=new Vector3(1,1,1);
			}
			
			transform.localPosition= new Vector3(local_position.x*grid_length.x,local_position.y*grid_length.y,local_position.z*grid_length.z);
			transform.localRotation=Quaternion.Euler(local_rotation);
			transform.localScale=local_scale;
		}
	}
}