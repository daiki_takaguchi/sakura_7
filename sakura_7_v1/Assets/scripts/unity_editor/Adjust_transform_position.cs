﻿using UnityEngine;
using System.Collections;

namespace SAKURA.UNITY_EDITOR{
[ExecuteInEditMode]
public class Adjust_transform_position : MonoBehaviour {
		/*
		public SAKURA.Consts.Unity_editor.Adjust_transform_position.Auto_adjust_mode auto_adjust_mode=Consts.Unity_editor.Adjust_transform_position.Auto_adjust_mode.local_position;

		public Vector3 local_position;
		[HideInInspector] public Vector3 local_position_backup;
		[HideInInspector] public bool adjust_local_position_now;

		public Vector3 global_position;
		[HideInInspector] public Vector3 global_position_backup;
		[HideInInspector] public bool adjust_global_position_now;



		private float original_grid_length;
		
		public bool change_grid_length=false;
		public float custom_grid_length=5.12f;
		[HideInInspector] public float custom_grid_length_backup;

	// Use this for initialization
		void Start () {
		
		}

		// Update is called once per frame
		void Update () {
			if(!Application.isPlaying){

				original_grid_length=GameObject.FindObjectOfType<Data>().game_settings.grid_length;
				custom_grid_length_backup=custom_grid_length;

				if(auto_adjust_mode==Consts.Unity_editor.Adjust_transform_position.Auto_adjust_mode.local_position){
					adjust_local_position_now=true;
				}else if(auto_adjust_mode==Consts.Unity_editor.Adjust_transform_position.Auto_adjust_mode.global_position){
					adjust_global_position_now=true;
				}

				if(adjust_global_position_now){

					global_position_backup=global_position;

					float temp=original_grid_length;

					if(change_grid_length){
						temp=custom_grid_length;
					}

					transform.position=global_position*temp;

					adjust_global_position_now=false;

				}

				if(adjust_local_position_now){

					local_position_backup=local_position;

					float temp=original_grid_length;
					
					if(change_grid_length){
						temp=custom_grid_length;
					}

					transform.localPosition=local_position*temp;

					adjust_local_position_now=false;
				}

			}
		}*/
	}
}
