﻿using System;


namespace System{
public class Csv_exception : Exception
	{
		public String ErrorCode = "";
		
		public Csv_exception() : base()
		{
		}
		
		public Csv_exception(string message, string code) : base(message)
		{
			this.ErrorCode = code;
		}
		public Csv_exception(string message, Exception inner, string code) : base(message, inner)
		{
			this.ErrorCode = code;
		}
	}
}