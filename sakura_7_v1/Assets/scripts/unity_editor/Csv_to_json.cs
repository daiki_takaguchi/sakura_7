using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using System.IO;
using System;

namespace SAKURA.UNITY_EDITOR{

	[ExecuteInEditMode]

	public class Csv_to_json : MonoBehaviour {

		public bool convert_file;

		public int empty_rows=3;
		public int empty_columns = 2;

		public string input_file_name;
		public string output_file_name;

		public File_name_list chara_list;
		public File_name_list sound_list;
		public File_name_list picture_list;


	


		void Update () {
			if(!Application.isPlaying){
				
				if(convert_file){

					run ();
				
					convert_file=false;
					
				}
				
			}
			
		}

		public void run(){

			List<List<string>> loaded_data = Csv_functions.load (input_file_name);

			print ("number of rows "+loaded_data.Count);

			JSONObject output = convert_data (loaded_data);


			FileStream fs = new FileStream(output_file_name,FileMode.Create, FileAccess.Write);
			
			StreamWriter sw = new StreamWriter (fs);

			sw.WriteLine(output.ToString());    
			
			sw.Flush();
			sw.Close(); 
			fs.Close ();

		
		}

		public JSONObject convert_data(List<List<string>> input_string){

			JSONObject output = new JSONObject(JSONObject.Type.OBJECT);

			int row_count = 0;

			print ("number of rows:"+ input_string.Count);

			int insert_count=0;

			List<JSONObject_insert> insert_row_list= new List<JSONObject_insert>();

			foreach(List<string> row in input_string){

				if(row_count>=empty_rows){
					/*
					string temp="";


					foreach (string column in row){
						if(){

						}
						temp+=column+",";
					}*/





				

					string action_name= command_to_action(row);

					if(action_name=="Action_person_image"){

		
						if(get_column_data(row,"text")!=""){
							
							List<string> temp=row;
							
							int index=get_column_no(temp,"name");
							
							JSONObject temp_json=row_to_json(row,"Action_adv_text");
							
							JSONObject_insert temp_insert= new JSONObject_insert("Action_adv_text",temp_json);
								
			
							insert_row_list.Add(temp_insert);
							
						}

					}

					if(action_name!=""){

						JSONObject j=row_to_json(row,action_name);

						print ("converting row "+row_count+" : "+j.ToString());

						output.AddField(action_name,j);
					
					}





					if(insert_row_list.Count>0){

						//if(insert_count<100){

							foreach(JSONObject_insert insert_row in insert_row_list){

							//	action_name= insert_row.key_name;
								
							//j	=insert_row.json;
								
							//	print ("converting row "+row_count+" : "+j.ToString());
								
								
								output.AddField(insert_row.key_name,insert_row.json);

								insert_count++;



							}
						 //}

						insert_row_list.Clear();


					}



				}else{
					print ("skipped row "+row_count);
				}

				row_count++;
			}

			return output;

		}




		public string command_to_action(List<string> input_row){

			string output = "";

			string command = get_column_data (input_row, "command").ToLower();
		
			switch(command){

				case "wait":
					output="Wait";
					break;

				case "se":

					output="Audio_SE";

					break;
					
				case "bgm":

					output="Audio_BGM";

					break;

				case "ambience":

					output="Audio_SE";

					break;

				case "bg":


					output="Action_image";

					break;
				case "pic":
					
					
					output="Action_image";
					
					break;

				case "fb":
					
					output="Action_image";

					break;

				case "scene":
					
					output="Jump";

					break;

				case "title":
					
					output="Jump";

					break;

				case "select":

					//output="Select";

					output="";

					break;

				case "beat":

					output="Effect";
					break;

				case "":

					if(get_column_data(input_row,"name")==""){

						output="Action_adv_text";

					}else if(get_column_data(input_row,"text")!=""){
						output="Action_person_image";

					}else{

						output="";

					}


					break;
			}

			return output;
		
		
		}
		

		public string convert_column_data(List<string> input_row,string input_action_name,string input_column_name,string input_variable_name){

			string output = "";
				
			string temp= get_column_data_exception( input_row,input_action_name,input_column_name, input_variable_name);
			
			if(temp==""){

				output=get_column_data( input_row,input_action_name,input_column_name);

			}else{
				output=temp;
			}

			return output;
		}

		
		public string get_column_data(List<string> input_row,string input_action_name,string input_column_name){

			string output = get_column_data (input_row,input_column_name);

			//print (input_action_name+input_column_name);


			if(input_action_name=="Action_image"&&input_column_name=="name"){

				output=picture_list.convert(output);

				output=output.Split('.')[0];
			}else if(input_action_name=="Audio_BGM"&&input_column_name=="name"){

				output=sound_list.convert("BGM",output);

				output=output.Split('.')[0];

			}else if(input_action_name=="Action_person_image"&&input_column_name=="name"){

				string para1 = get_column_data (input_row,"para1");

				if(para1==""){
					para1="通常";
				}

				print ("chara file "+output+" "+para1);

				output=chara_list.convert(para1,output);
				
				output=output.Split('.')[0];

				print (output);

			}

			return output;
		}

		public string get_column_data(List<string> input_row,string input_column_name){


			string output="";

			int index=get_column_no(input_row,input_column_name);

			if(index<input_row.Count){
				output=input_row[index];
			}

			return output;
		
		}


		public int get_column_no(List<string> input_row,string input_column_name){

			List<string> column_names = new List<string> ();

			for(int i=0;i<empty_columns;i++){
				column_names.Add ("empty");
			}

			column_names.Add ("command");
			column_names.Add ("name");
			column_names.Add ("text");
			column_names.Add ("para1");
			column_names.Add ("para2");
			column_names.Add ("para3");
			column_names.Add ("x");
			column_names.Add ("y");
			column_names.Add ("time");
			column_names.Add ("no");
			column_names.Add ("tag");
			//column_names.Add ("person_name");


			int output=0;



			foreach( string column_name in column_names){

				if(column_name==input_column_name){


					break;

				}

				output++;

			}


			return output;

		

		}

		public string get_column_data_exception(List<string> input_row,string input_action_name,string input_column_name,string input_variable_name){
			string output = "";

			string command = get_column_data (input_row, "command").ToLower();

			switch(command){

				case "beat":

				
					break;

				case "bg":
					switch(input_column_name){
						case "command":
							output="show";
							break;
						case "layer":
							output="background";
							break;
					}
					break;
				case "pic":
					switch(input_column_name){
						case "command":

							string temp_name=get_column_data (input_row, "name");

							if(temp_name==""){
								output="hide";
							}else{
								output="show";
							}



							break;

						case "layer":

							string para1 = get_column_data (input_row, "para1").ToLower();

							output="still_1";

							switch(para1){
								case "2":

								break;
							}

						

							break;
						
					}
					break;
					
				case "bgm":
		
					switch(input_column_name){
					case "command":
						output="play";
						break;
					}
					break;
				case "fb":
					switch(input_column_name){
						case "command":
							output="change";
							break;
						case "layer":
							output="background";
							break;
					}
					break;

				case "scene":
					if (input_column_name == "command") {
						output = "scene";
					}

					break;

				case "":

					switch(input_action_name){

						case "Action_person_image":

							switch(input_column_name){
								case "command":
									output="show";
									break;

								
									
								case "x":
									
									output= get_column_data (input_row, "x").ToUpper();
									
									try{
										
										Enum.Parse(typeof(Consts.Data.Game_event.Action.Person_image_position),output);
										
									}catch{
										
										output="";
									}
									
									if(output==""){
										output="C";
									}
									
									break;
									
									
									

							}

							break;
							
					}
					break;

			}





			return output;

		}


		public JSONObject row_to_json(List<string> input_row, string input_action_name){
			JSONObject output = new JSONObject (JSONObject.Type.ARRAY);

			List<column_name_to_variable_name> temp= get_column_names_to_variable_names_list(input_action_name);

			foreach(column_name_to_variable_name c in temp){

				output.AddField(c.variable_name,convert_column_data(input_row,input_action_name,c.column_name,c.variable_name));

			}


			return output;
		
		}

		public List<column_name_to_variable_name> get_column_names_to_variable_names_list(string input_action_name){

			List<column_name_to_variable_name> output = new List<column_name_to_variable_name> ();

			switch(input_action_name){
				case  "Action_adv_text":

					output.Add (new column_name_to_variable_name ("name", "person_name_string"));


					output.Add (new column_name_to_variable_name ("text", "text"));

					break;

				case  "Action_image":

					output.Add (new column_name_to_variable_name ("name", "image_name"));
				
					output.Add (new column_name_to_variable_name ("layer", "image_layer_type"));

					output.Add (new column_name_to_variable_name ("command", "image_operation"));


					break;

				case  "Action_person_image":
					
					output.Add (new column_name_to_variable_name ("name", "image_name"));


					
					output.Add (new column_name_to_variable_name ("x", "position"));
					
					output.Add (new column_name_to_variable_name ("command", "image_operation"));
					
					
					break;

				case  "Audio_BGM":
					
					output.Add (new column_name_to_variable_name ("name", "BGM_name"));
					
					output.Add (new column_name_to_variable_name ("command", "audio_operation"));
				
				
					break;



				case  "Effect":

					output.Add (new column_name_to_variable_name ("command", "effect_type"));

					break;

				case "Wait":

					output.Add (new column_name_to_variable_name ("time", "wait_time"));
					break;

				case "Jump":
				
					output.Add (new column_name_to_variable_name ("command", "jump_type"));

					output.Add (new column_name_to_variable_name ("para1", "destination"));
					break;

				case "Select":

					output.Add (new column_name_to_variable_name ("text", "select_text_0"));
					output.Add (new column_name_to_variable_name ("text", "select_text_1"));
					output.Add (new column_name_to_variable_name ("text", "select_text_2"));
					break;

				/*
				output.Add (new column_name_to_variable_name ("text", "jump_tag_0"));
				output.Add (new column_name_to_variable_name ("text", "jump_tag_0"));
				output.Add (new column_name_to_variable_name ("text", "jump_tag_0"));*/

			}

			output.Add (new column_name_to_variable_name("tag","action_tag"));
			

			return output;

			
			
		}


	}

	public class column_name_to_variable_name {

		public string column_name;

		public string variable_name;

		public column_name_to_variable_name(string input_column_name, string input_variable_name){
			column_name = input_column_name;
			variable_name = input_variable_name;
		}


	}

	public class JSONObject_insert {
		
		public string key_name;
		
		public JSONObject json;
		
		public JSONObject_insert(string input_key_name, JSONObject input_json){
			key_name=input_key_name;
			json=input_json;
		}
		
		
	}
	

}
