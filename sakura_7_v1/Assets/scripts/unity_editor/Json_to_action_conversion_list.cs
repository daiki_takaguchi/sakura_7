﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using System.Reflection;
using A=SAKURA.DATA.GAME_EVENT.ACTION;

namespace SAKURA.UNITY_EDITOR{

	[ExecuteInEditMode]
	public class Json_to_action_conversion_list : MonoBehaviour {

		public bool export_structure_json;

		public string json_file_name;

		public List<Json_to_action_conversion> json_to_action_conversion_list ;


		
		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if(!Application.isPlaying){

				if(export_structure_json){

					save_structure(json_file_name);

					export_structure_json=false;
				}

			}
		
		}

		public void convert(){

		}

		public void convert_exception(string action_name,string data){



		}

		public void save_data(string file_name,List<A.Action> input_action_list){


			FileStream fs = new FileStream(file_name,FileMode.Create, FileAccess.Write);
			
			StreamWriter sw = new StreamWriter (fs);
			
			JSONObject j = new JSONObject(JSONObject.Type.OBJECT);


			foreach (A.Action action in input_action_list) {


				bool is_finished=false;
				string action_name="";

				foreach (Json_to_action_conversion j_to_a in json_to_action_conversion_list) {

					if(action.GetType().Name==j_to_a.action_name){

						action_name=j_to_a.action_name;

						int count=0;

						JSONObject arr = new JSONObject(JSONObject.Type.ARRAY);

						foreach (string variable_name in j_to_a.variable_name){

							Type variable_type=variable_type_to_type( j_to_a.variable_type[count]);


							string value= Sakura.get_variable_value(action,action.GetType(),variable_name,variable_type);

							

							//JSONObject data = new JSONObject(JSONObject.Type.OBJECT);

							arr.AddField(variable_name,value);





							count++;


						}

						j.AddField(action_name,arr);

						is_finished=true;

						break;
					}

				}


				if(is_finished){

				}else{
					Debug.LogError("conversion error!");
				}


			}


			//print (j.ToString());

			sw.WriteLine(j.ToString());    

			sw.Flush();
			sw.Close(); 
			fs.Close ();



		}

		public void load_data(string file_name,A.Action_list input_action_list){

			List<A.Action> output = new List<A.Action> ();

			string json = "";
			
			try 
			{
				if (File.Exists(file_name)) {
					
					print ("loading "+file_name);
					
					StreamReader sr = new System.IO.StreamReader(file_name);
					
					json=sr.ReadToEnd();

					print ("json: "+json);

				}else{
					Debug.LogError("file does not exist!");
				}
				
			}catch{
			}

			JSONObject j = new JSONObject(json);

			for(int i = 0; i < j.list.Count; i++){
				string key = (string)j.keys[i];
				JSONObject one = (JSONObject)j.list[i];

				decode(key,one,input_action_list);
			}

			foreach(JSONObject one in j.list){




			}


		}

		public void decode(string input_action_name,JSONObject input_json,A.Action_list input_action_list){

			string action_name_prefix= "SAKURA.DATA.GAME_EVENT.ACTION.";

			if(input_action_name!=""){

				A.Action new_action =input_action_list.add_action(Type.GetType(action_name_prefix+input_action_name));

				//List<String> variable_name_list= new List<string>();
				//List<String> value_list= new List<string>();


				Json_to_action_conversion conversion=find_list(input_action_name);


				for(int i = 0; i < input_json.list.Count; i++){

					JSONObject j=input_json.list[i];

					string variable_name=input_json.keys[i];
					string variable_value="";

					switch(j.type){

						case JSONObject.Type.OBJECT:

							break;
						case JSONObject.Type.ARRAY:

							break;
						case JSONObject.Type.STRING:
							variable_value=j.str;
							break;
						case JSONObject.Type.NUMBER:
							variable_value=j.n.ToString();
							break;
						case JSONObject.Type.BOOL:
							//Debug.Log(obj.b);
							break;
						case JSONObject.Type.NULL:
							//Debug.Log("NULL");
							break;
						
					}

					//if(has_variable_name(conversion,variable_name){

					set_variable(new_action,variable_name,variable_value);
					//}




				}



			}

		}

		public Json_to_action_conversion find_list(string input_action_name){

			Json_to_action_conversion output = null;


			foreach (Json_to_action_conversion one in json_to_action_conversion_list) {
				if(one.action_name==input_action_name){

					return one;
				}
			}



			return output;
		
		}
		/*
		public bool has_variable_name(Json_to_action_conversion input_conversion, string input_variable_name){

			
		
		}*/



		public void set_variable(A.Action input_action ,string input_variable_name,string input_value){

			FieldInfo info =input_action.GetType().GetField(input_variable_name);

			if(info!=null){

				//cannot use switch code



				if(info.FieldType==typeof(string)){
					info.SetValue(input_action,input_value);
				}else if(info.FieldType==typeof(float)){

					float result=0;

					if(float.TryParse(input_value,out result)){
						info.SetValue(input_action,result);
					}else{
						Debug.LogError("value is not float!");
					};


				}else if(info.FieldType.IsEnum){
					if(input_value!=""){
						info.SetValue(input_action,Enum.Parse(info.FieldType,input_value));
					}

				}else{
					Debug.LogError("aaaaaaaaaaaaa"+info.FieldType.ToString());
				}

			
			}

		
		}


		public void save_structure(string input_data_name){
			
			
	
	
			FileStream fs = new FileStream(input_data_name,FileMode.Create, FileAccess.Write);
			
			StreamWriter sw = new StreamWriter (fs);
			
			int count = 0;
			
			JSONObject j = new JSONObject(JSONObject.Type.OBJECT);
			
			
			foreach(Json_to_action_conversion temp in json_to_action_conversion_list){
				
				if(temp!=null){

					int action_count=0;

					JSONObject action_data = new JSONObject(JSONObject.Type.ARRAY);

					foreach(string variable_name in temp.variable_name){

						JSONObject arr=variable_type_to_json(variable_name,temp.variable_type[action_count]);
						
						action_data.AddField(action_count.ToString(),arr);

						action_count++;

					

					}


					j.AddField(temp.action_name,action_data);

					
					
				}


				
				count++;
				
			}
			sw.WriteLine ("var data_structure =");
			sw.WriteLine(j.ToString());     
			
			
			sw.Flush();
			sw.Close(); 
			fs.Close ();
			
		}

		public JSONObject variable_type_to_json(string variable_name, SAKURA.Consts.Unity_editor.Json_to_action_conversion.Variable_type input_type){

			Type temp = variable_type_to_type (input_type);

			JSONObject output = new JSONObject(JSONObject.Type.OBJECT);

			JSONObject data_name = new JSONObject(JSONObject.Type.ARRAY);

			data_name.AddField ("text",variable_name);

			output.AddField("data_name",data_name);

			if(temp.IsEnum){


				JSONObject arr = new JSONObject(JSONObject.Type.ARRAY);

				int count=0;

				foreach (string arr_name in Enum.GetNames(temp)) {

					JSONObject arr_option = new JSONObject(JSONObject.Type.OBJECT);

					arr_option.AddField("value",arr_name);

					arr_option.AddField("text",arr_name);


					arr.AddField(count.ToString(),arr_option);

					count++;

				}

				output.AddField("data_type","enum" );


				output.AddField("data",arr );



			}else if(temp ==typeof(string)){



				output.AddField("data_type","string" );

			}else if(temp ==typeof(float)){


				output.AddField("data_type","string" );
			}

			return output;

		}




		public Type variable_type_to_type(SAKURA.Consts.Unity_editor.Json_to_action_conversion.Variable_type input_type){

			Type output = null;

			switch(input_type){
				case SAKURA.Consts.Unity_editor.Json_to_action_conversion.Variable_type.string_type:
					output= typeof(string);
				break;

				case Consts.Unity_editor.Json_to_action_conversion.Variable_type.float_type:
				output= typeof(float);

				break;
			
			}

			return output;
		}

		

	
	}
}
