﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using System.IO;

namespace SAKURA.UNITY_EDITOR{

	[ExecuteInEditMode]

	public class File_name_list : MonoBehaviour {

		public string reference;

		public bool load;

		public List<string> ref_name;
		public List<string> pattern;
		public List<string> file_name;

		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {

			if(load){

				ref_name= new List<string>();
				pattern= new List<string>();
				file_name= new List<string>();

				load_csv(reference);

				load=false;
			}

		}

		public string convert(string input_ref_name){

			string output = "";

			for(int i=0;i<ref_name.Count;i++){
				if(input_ref_name==ref_name[i]){
					output=file_name[i];
				}

			}
			return output;
		}

		public string convert(string input_pattern ,string input_ref_name){
			
			string output = "";
			
			for(int i=0;i<ref_name.Count;i++){
				if(input_ref_name==ref_name[i]){
					if(input_pattern==pattern[i]){
						output=file_name[i];
					}

				}
				
			}
			return output;
		}


		public void load_csv(string input_file_name){

			List<List<string>> temp=Csv_functions.load(input_file_name);

			int row_count = 0;

			foreach(List<string> row in temp){


				if(row_count>1){

					int column_count=0;

					foreach(string column in row){

						switch(column_count){

							case 1:

								ref_name.Add (column);

								break;

							case 2:

								pattern.Add (column);
							
								break;

							case 3:

								file_name.Add (column);
							
								break;

						}


						column_count++;

					}

				}

				row_count++;

			}



		}

	}

}
