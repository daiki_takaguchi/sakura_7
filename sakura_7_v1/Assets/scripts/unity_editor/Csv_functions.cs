﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using System.IO;

namespace SAKURA.UNITY_EDITOR{

	public class Csv_functions : MonoBehaviour {




		private static List<string> load_file(string file_name){

			List<string> output = new List<string>();

			string line;

			//print (file_name);

			try 
			{
				if (File.Exists(file_name)) {

					print ("loading "+file_name);

					StreamReader sr = new System.IO.StreamReader(file_name);

					while((line = sr.ReadLine()) != null){
						//print (line);
						//print ("raw file row :" +line);
						output.Add(line);
					}
				}else{
					Debug.LogError("file does not exist!");
				}

			}catch{
			}
				


		

			return output;
		}

		private static StreamWriter write_file(string file_name, bool append){

			StreamWriter output = new StreamWriter (file_name, append);

			return output;

		}

		public static List<string> load_one_row(string one_row){
			List<string> output = new List<string> ();

			string temp = one_row;

			bool is_data_string = false;
			bool is_double_quoted_data_string = false;
			bool is_prev_char_double_quote = false;

			string column_data = "";

//			/bool is_end=false;


			for (int i = 0; i < temp.Length; i++){

				if(is_data_string){

					if(is_double_quoted_data_string){

						if(is_prev_char_double_quote){

							if(temp[i]=='\"'){
								is_prev_char_double_quote =false;
								column_data+="\"";

							} else if(temp[i]==','){

								output.Add (column_data);

								is_data_string=false;

								is_double_quoted_data_string=false;

							}else if(temp[i]=='\r' ||temp[i]=='\n'){

								//throw new Csv_exception("csv parse error!","1");
								output.Add (column_data);
								is_data_string=false;

								is_double_quoted_data_string=false;

								//is_end=true;
								break;
								
							}else{



								throw new Csv_exception("csv parse error!","1");




							}
						}else{

							if(temp[i]=='\"'){

								if(i==temp.Length-1){
									output.Add (column_data);
									is_data_string=false;
									
									is_double_quoted_data_string=false;
									

									break;

								}else{
									is_prev_char_double_quote =true;
								}

							}else{
								is_prev_char_double_quote =false;
								column_data+=temp[i];
							}
						}

					}else{

						if(temp[i]==','){

							output.Add (column_data);
							is_data_string=false;

						}else if(i==temp.Length-1){
							column_data+=temp[i];
							output.Add (column_data);
							is_data_string=false;
							break;

						}else if(temp[i]=='\r' ||temp[i]=='\n'){

							//print (row_data);
							output.Add (column_data);
							//is_end=true;
							break;


						}else{
						
							column_data+=temp[i];

						}



					}
				
				
				}else{



					is_prev_char_double_quote =false;
					column_data="";

				

					if(temp[i]=='\"'){


						is_data_string=true;
						
						is_double_quoted_data_string=true;

					}else if (temp[i]==',') {


						output.Add(column_data);

						is_data_string=false;

						is_double_quoted_data_string=false;

					}else if (i==temp.Length-1) {
						
						

						Debug.LogError("unexpected error!");

						

					}else if(temp[i]=='\r' ||temp[i]=='\n'){

						//is_end=true;
						break;
					}else{

						column_data+=temp[i];

						is_data_string=true;
						
						is_double_quoted_data_string=false;
						

					}



				}
			
			}
			/*
			if(!is_end){

				Debug.LogError("parse error: "+one_row);
				Debug.LogError("parse error: "+one_row);

				throw new Csv_exception("parse error!","1");
			}*/

			//print (column_data+is_end);
			//print (temp[temp.Length-1]);
			//output.Add (row_data);

		

			if(is_double_quoted_data_string&&!is_prev_char_double_quote){

				throw new Csv_exception("double quote error!","1");

			}

			//print ("row:"+row_data);


			return output;


		}

		public static List<string> split_by_first_comma(string input_string){
			List<string> output = new List<string> ();



			return output;

		}

		public static string save_one_row(List<string> one_row){
			string output = "\"";

			bool is_first_row = true;

			foreach(string one_data in one_row){

				if(is_first_row){
					is_first_row=false;
					
				}else{
					output+="\",\"";
				}

				output+=one_data.Replace("\"","\"\"");


			}

			output += "\"\r\n";

			return output;
		}


		public static List<List<string>>load(string file_name){

			List<List<string>> output = new List<List<string>> ();

			List<string> loaded_file =load_file (file_name);

			string prev_text = "";

			print ("number of rows "+loaded_file.Count);

			for (int i = 0; i < loaded_file.Count; i++){

				try{

					List<string> temp_row=load_one_row(prev_text+loaded_file[i]);

					string row="";

					foreach(string column in temp_row){
						row+=column+",";
					}
					print ("raw file row "+i+" : "+loaded_file[i]);
					print ("csv loading row "+i+" : "+row);

					output.Add(temp_row);
					/*
					string ss = "";
					foreach (string s in temp_row) {
						ss+=s;
					}

					print (ss);*/


					prev_text="";

				}catch (Csv_exception ex){

					prev_text+=loaded_file[i];

					Debug.LogError ("error:" +ex.Message);
				}

			}
			

			



			return output;

		}



		public static bool add(string file_name, List<string> data){

			bool output = true;


			return output;
		}



		public static bool over_write(string file_name, List<List<string>> data){

			bool output = true;
			
			
			return output;

		}


	}


}
